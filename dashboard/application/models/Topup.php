<?php
class Topup extends CI_Model{

    // get from production
	function get_all()
    {
        $query = $this->db->query("SELECT
        (select sum(processed_amount) from icp_self_payment_request where date_of_transfer = curdate() and request_type='offline') as 'total_manual',
        (select sum(processed_amount) from icp_self_payment_request where date_of_transfer = curdate() and request_type='offline' and dest_bank_name ='BANK BCA' group by dest_bank_name) as 'manualbca',
        (select sum(processed_amount) from icp_self_payment_request where date_of_transfer = curdate() and request_type='offline' and dest_bank_name ='BANK Mandiri' group by dest_bank_name)  as 'manualmandiri',
        (select sum(processed_amount) from icp_self_payment_request where date_of_transfer = curdate() and request_type='offline' and dest_bank_name ='BANK Permata' group by dest_bank_name)  as 'manualpermata',
        (select sum(processed_amount) from icp_self_payment_request where date_of_transfer = curdate() and request_type='offline' and dest_bank_name ='BANK BRI' group by dest_bank_name) as 'manualbri',
        (select sum(processed_amount) from icp_self_payment_request where date_of_transfer = curdate() and request_type='offline' and dest_bank_name ='BANK SINARMAS' group by dest_bank_name) as 'manualsinarmas',
        (select sum(processed_amount) from icp_self_payment_request where date_of_transfer = curdate() and request_type='TOPUPTICKET') as 'totalticket',
        (select sum(processed_amount) from icp_self_payment_request where date_of_transfer = curdate() and request_type='TOPUPTICKET' and dest_bank_name ='BANK BCA' group by dest_bank_name) as 'tiketbca',
        (select sum(processed_amount) from icp_self_payment_request where date_of_transfer = curdate() and request_type='TOPUPTICKET' and dest_bank_name ='BANK Mandiri' group by dest_bank_name) as 'tiketmandiri',
        (select sum(processed_amount) from icp_self_payment_request where date_of_transfer = curdate() and request_type='Bank Wire Transfer') as 'totalvirtual'
        
        from icp_self_payment_request limit 1");
        return $query->result_array();
	}

    // Insert data from production to local
    function insert_data($total, $bca, $mandiri, $permata, $bri, $sinarmas, $totalticket, $tiketbca, $tiketmandiri, $totalvirtual){
        $CI = &get_instance();
        $this->db = $CI->load->database('dashboard', TRUE);
        $query = $this->db->query("INSERT INTO TOP_UP (total_manual, manualbca, manualmandiri, manualpermata, manualbri, manualsinarmas, totalticket, tiketbca, tiketmandiri, totalvirtual, tanggal) VALUES ($total, $bca, $mandiri, $permata, $bri, $sinarmas, $totalticket, $tiketbca, $tiketmandiri, $totalvirtual, NOW() )");
        echo "Data Topup Berhasil di Insert ";
    }

    // Update data from production to local
    function update_data($total, $bca, $mandiri, $permata, $bri, $sinarmas, $totalticket, $tiketbca, $tiketmandiri, $totalvirtual){
        $CI = &get_instance();
        $this->db = $CI->load->database('dashboard', TRUE);
        $query = $this->db->query("UPDATE TOP_UP SET total_manual = $total , manualbca = $bca, manualmandiri = $mandiri, manualpermata = $permata, manualbri = $bri, manualsinarmas = $sinarmas, totalticket = $totalticket, tiketbca = $tiketbca, tiketmandiri = $tiketmandiri, totalvirtual = $totalvirtual ,tanggal = NOW() WHERE date(tanggal) = date(now()) ORDER BY tanggal DESC LIMIT 1");
        echo "Data Topup Berhasil di Update";
    }

    // Get the Local data
    function get_data_local(){
        $CI = &get_instance();
        $this->db = $CI->load->database('dashboard', TRUE);
        $query = $this->db->query("select * from TOP_UP ORDER BY tanggal DESC LIMIT 1");
        return $query->result_array();
    }





	// get from production
    function get_virtual()
    {
        $CI = &get_instance();
        $this->db3 = $CI->load->database('kiosonbankpayments', TRUE);
        $query = $this->db3->query("SELECT
            (select count(TRANS_ID) from KIOSON_SGO_BANK_WIRE_TRANSFER where date(created_date) = curdate() and KIOSON_PAY_STATUS = 'SUCCESS') as virtualbri,
            (select sum(payment_amount) from KIOSON_SGO_BANK_WIRE_TRANSFER where date(created_date) = curdate() and KIOSON_PAY_STATUS = 'SUCCESS') as 'virtualamountbri',
            (select count(TRANS_ID) as virtualsinarmas from KIOSON_BANK_WIRE_TRANSFER where date(created_date) = curdate() and KIOSON_PAY_STATUS = 'SUCCESS') as 'virtualsinarmas',
            (select sum(payment_amount) as 'virtualamountsinarmas' from KIOSON_BANK_WIRE_TRANSFER where date(created_date) = curdate() and KIOSON_PAY_STATUS = 'SUCCESS') as 'virtualamountsinarmas'
            
        from KIOSON_SGO_BANK_WIRE_TRANSFER limit 1");
        return $query->result_array();
    }


    // Insert data from production to local
    function insert_data_virtual($bri, $amount_bri, $sinarmas, $amount_sinarmas){
        $CI = &get_instance();
        $this->db = $CI->load->database('dashboard', TRUE);
        $query = $this->db->query("INSERT INTO VIRTUAL_TOP_UP (virtualbri, virtualamountbri, virtualsinarmas, virtualamountsinarmas, tanggal) VALUES ($bri, $amount_bri, $sinarmas, $amount_sinarmas, NOW() )");
        echo "Data Topup Virtual Berhasil di Insert";
    }

    // Update data from production to local
    function update_data_virtual($bri, $amount_bri, $sinarmas, $amount_sinarmas){
        $CI = &get_instance();
        $this->db = $CI->load->database('dashboard', TRUE);
        $query = $this->db->query("UPDATE VIRTUAL_TOP_UP SET virtualbri = $bri , virtualamountbri = $amount_bri, virtualsinarmas = $sinarmas, virtualamountsinarmas = $amount_sinarmas, tanggal = NOW() WHERE date(tanggal) = date(now()) ORDER BY tanggal DESC LIMIT 1");
        echo "Data Topup Virtual Berhasil di Update";
    }


    // Get the Local data
    function get_data_virtual(){
        $CI = &get_instance();
        $this->db = $CI->load->database('dashboard', TRUE);
        $query = $this->db->query("select * from VIRTUAL_TOP_UP ORDER BY tanggal DESC LIMIT 1 ");
        return $query->result_array();
    }
    
    // Check Lastdate on database
    public function check_date($param){
        $CI = &get_instance();
        $this->db = $CI->load->database('dashboard', TRUE);
        $query = $this->db->query("SELECT date(tanggal) as tanggal from $param where date(tanggal) = curdate() ORDER BY tanggal DESC LIMIT 1");
        return $query->result_array();
    }
    
}
