<?php
class Transaksi extends CI_Model{

	// Pulsa dan Paket
    

    public function get_total_today($type)
    {
        
        $query = $this->db->query("SELECT
        (select count(transaction_id) from ipay_transactions 
        where transaction_date_only = curdate() 
        and ((payment_status = 'RECEIVED' and order_status = 'SUCCESS') or (payment_status = 'REFUND' and order_status = 'FAILED')) and transaction_type = '$type') as 'total' ,
        (select count(transaction_id) from ipay_transactions 
        where transaction_date_only = curdate() 
        and order_status = 'SUCCESS' and payment_status = 'RECEIVED' and transaction_type = '$type') as 'sukses' ,
        (select count(transaction_id) from ipay_transactions 
        where transaction_date_only = curdate() 
        and order_status = 'failed' and payment_status = 'REFUND' and transaction_type = '$type') as 'gagal' 
         
        from ipay_transactions limit 1");
        return $query->result_array();
    }

    public function get_total_this_month()
    {
        
        $query = $this->db->query("SELECT 
                COUNT(
                    IF( transaction_date_only BETWEEN DATE_FORMAT(NOW(), '%Y-%m-01') AND curdate()
                        and order_status = 'SUCCESS' and payment_status = 'RECEIVED' 
                        and transaction_type = 'MR'
                        ,1,NULL)
                    ) AS MR_sukses,
                COUNT(
                    IF(transaction_date_only BETWEEN DATE_FORMAT(NOW(), '%Y-%m-01') AND curdate()
                        and order_status = 'failed' and payment_status = 'REFUND' 
                        and transaction_type = 'MR'
                    ,1,NULL)
                ) AS MR_gagal,
                
                COUNT(
                    IF( transaction_date_only BETWEEN DATE_FORMAT(NOW(), '%Y-%m-01') AND curdate()
                        and order_status = 'SUCCESS' and payment_status = 'RECEIVED' 
                        and transaction_type = 'DC'
                        ,1,NULL)
                    ) AS DC_sukses,
                COUNT(
                    IF(transaction_date_only BETWEEN DATE_FORMAT(NOW(), '%Y-%m-01') AND curdate()
                        and order_status = 'failed' and payment_status = 'REFUND' 
                        and transaction_type = 'DC'
                    ,1,NULL)
                ) AS DC_gagal
            from ipay_transactions");
        return $query->result_array();
    }

    public function get_total_last_month()
    {
        
        $query = $this->db->query("SELECT 
                COUNT(
                    IF( transaction_date_only BETWEEN DATE_FORMAT(NOW(), '%Y-%m-01')-interval 1 month 
                        AND curdate()-interval 1 month
                        and order_status = 'SUCCESS' and payment_status = 'RECEIVED' 
                        and transaction_type = 'MR'
                        ,1,NULL)
                    ) AS MR_sukses,
                COUNT(
                    IF(transaction_date_only BETWEEN DATE_FORMAT(NOW(), '%Y-%m-01')-interval 1 month 
                        AND curdate()-interval 1 month
                        and order_status = 'failed' and payment_status = 'REFUND' 
                        and transaction_type = 'MR'
                    ,1,NULL)
                ) AS MR_gagal,
                
                COUNT(
                    IF( transaction_date_only BETWEEN DATE_FORMAT(NOW(), '%Y-%m-01')-interval 1 month 
                        AND curdate()-interval 1 month
                        and order_status = 'SUCCESS' and payment_status = 'RECEIVED' 
                        and transaction_type = 'DC'
                        ,1,NULL)
                    ) AS DC_sukses,
                COUNT(
                    IF(transaction_date_only BETWEEN DATE_FORMAT(NOW(), '%Y-%m-01')-interval 1 month 
                        AND curdate()-interval 1 month
                        and order_status = 'failed' and payment_status = 'REFUND' 
                        and transaction_type = 'DC'
                    ,1,NULL)
                ) AS DC_gagal
            from ipay_transactions");
        return $query->result_array();
    }

    
	
	// PLN dan PDAM
	function get_PLN()
    {
        $CI = &get_instance();
        $this->db2 = $CI->load->database('ipex', TRUE);
        $query=$this->db2->query("SELECT
                        COUNT(
                            IF(b.BILL_TYPE = 'PLN Prepaid'
                                and ((a.payment_status = 'RECEIVED' and a.order_status = 'SUCCESS')
                                or (a.payment_status = 'REFUND' and a.order_status = 'FAILED'))
                                ,1,NULL)
                            ) as total_prepaid,
                        COUNT(
                            IF(b.bill_type = 'PLN Prepaid' 
                                and a.order_status ='SUCCESS' and a.payment_status = 'RECEIVED'
                                ,1,NULL)
                            ) as prepaidsukses,
                        COUNT(
                            IF(b.bill_type = 'PLN Prepaid' 
                                and a.order_status ='FAILED' and a.payment_status = 'REFUND'
                                ,1,NULL)
                            ) as prepaidgagal,
                        COUNT(
                            IF(b.bill_type = 'PLN Postpaid' and
                            ((a.payment_status = 'RECEIVED' and a.order_status = 'SUCCESS') 
                            or (a.payment_status = 'REFUND' and a.order_status = 'FAILED'))
                                ,1,NULL)
                            ) as total_postpaid,
                         COUNT(
                            IF(b.bill_type = 'PLN Postpaid' 
                                and a.order_status ='SUCCESS' and a.payment_status = 'RECEIVED'
                                ,1,NULL)
                            ) as postpaidsukses,
                        COUNT(
                            IF(b.bill_type = 'PLN Postpaid' 
                                and a.order_status ='FAILED' and a.payment_status = 'REFUND'
                                ,1,NULL)
                            ) as postpaidgagal
                    from IPEX_TRANS_MASTER a 
                    inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID 
                    where a.TRANS_DATE_ONLY = curdate()");
        return $query->result_array();
    }

    // function PLN_lastmonth(){
    //     $CI = &get_instance();
    //     $this->db2 = $CI->load->database('ipex', TRUE);
    //     // $query=$this->db2->query("select count(IPEX_TRANS_ID) as '$lastalias' from IPEX_TRANS_MASTER a 
    //     // inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID 
    //     // where TRANS_DATE_ONLY between date_format(now(),'%Y-%m-01') -interval 1 month and curdate()- INTERVAL 1 month and bill_type = '$bill_type'");
    //     $query = $this->db2->query("SELECT 
    //                         COUNT(
    //                             IF(b.BILL_TYPE = 'PLN Prepaid'
    //                                 and b.STATUS != 'PENDING'
    //                                 ,1,NULL)
    //                             ) as total_prepaid,
    //                         COUNT(
    //                             IF(b.bill_type = 'PLN Postpaid' and
    //                             b.STATUS != 'PENDING'
    //                                 ,1,NULL)
    //                             ) as total_postpaid
    //                     from IPEX_TRANS_MASTER a 
    //                     inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID 
    //                     where a.TRANS_DATE_ONLY between date_format(now(),'%Y-%m-01') -interval 1 month 
    //                     and curdate()- INTERVAL 1 month");
    //     return $query->result_array();
    // }

    // function PLN_thismonth(){
    //     $CI = &get_instance();
    //     $this->db2 = $CI->load->database('ipex', TRUE);
    //     // $query=$this->db2->query("select count(IPEX_TRANS_ID) as '$thisalias' from IPEX_TRANS_MASTER a 
    //     // inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID 
    //     // where TRANS_DATE_ONLY between date_format(now(),'%Y-%m-01')and curdate() and bill_type = '$bill_type'");
    //     $query=$this->db2->query("SELECT 
    //                         COUNT(
    //                             IF(b.BILL_TYPE = 'PLN Prepaid'
    //                                 and b.STATUS != 'PENDING'
    //                                 ,1,NULL)
    //                             ) as total_prepaid,
    //                         COUNT(
    //                             IF(b.bill_type = 'PLN Postpaid' and
    //                             b.STATUS != 'PENDING'
    //                                 ,1,NULL)
    //                             ) as total_postpaid
    //                     from IPEX_TRANS_MASTER a 
    //                     inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID 
    //                     where a.TRANS_DATE_ONLY between date_format(now(),'%Y-%m-01')
    //                     and curdate()");
    //     return $query->result_array();
    // }

    function PLN_lastmonth_10(){
        $CI = &get_instance();
        $this->db2 = $CI->load->database('ipex', TRUE);
        $query = $this->db2->query("SELECT 
                             COUNT(
                                 IF(b.BILL_TYPE = 'PLN Prepaid'
                                     and b.STATUS != 'PENDING'
                                     ,1,NULL)
                                 ) as prepaid1,
                             COUNT(
                                 IF(b.bill_type = 'PLN Postpaid' and
                                 b.STATUS != 'PENDING'
                                     ,1,NULL)
                                 ) as postpaid1
                         from IPEX_TRANS_MASTER a 
                         inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID 
                         where a.TRANS_DATE_ONLY between date_format(now(),'%Y-%m-01') -interval 1 month 
                         and curdate()- INTERVAL 1 month");
        return $query->result_array();
    }

    function PLN_lastmonth_20(){
        $CI = &get_instance();
        $this->db2 = $CI->load->database('ipex', TRUE);
        $query = $this->db2->query("SELECT

        (select count(IPEX_TRANS_ID) from IPEX_TRANS_MASTER a 
        inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID 
        where trans_date_only 
        BETWEEN date_format(now(), '%Y-%m-01')-interval 1 month and date_format(now(), '%Y-%m-10')-interval 1 month
        and left (bill_type,7) = 'PLN Pre' 
        and ((a.payment_status = 'RECEIVED' and a.order_status = 'SUCCESS')
                                or (a.payment_status = 'REFUND' and a.order_status = 'FAILED'))) as 'prepaid1',
        
        (select count(IPEX_TRANS_ID) from IPEX_TRANS_MASTER a 
        inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID 
        where trans_date_only 
        BETWEEN date_format(now(), '%Y-%m-11')-interval 1 month and curdate()-interval 1 month
        and left (bill_type,7) = 'PLN Pre' 
        and ((a.payment_status = 'RECEIVED' and a.order_status = 'SUCCESS')
                                or (a.payment_status = 'REFUND' and a.order_status = 'FAILED'))) as 'prepaid2',

         
         (select count(IPEX_TRANS_ID) from IPEX_TRANS_MASTER a 
        inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID 
        where trans_date_only 
        BETWEEN date_format(now(), '%Y-%m-01')-interval 1 month and date_format(now(), '%Y-%m-10')-interval 1 month
        and left (bill_type,7) = 'PLN Pos' 
        and ((a.payment_status = 'RECEIVED' and a.order_status = 'SUCCESS')
                                or (a.payment_status = 'REFUND' and a.order_status = 'FAILED'))) as 'postpaid1',
        
        (select count(IPEX_TRANS_ID) from IPEX_TRANS_MASTER a 
        inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID 
        where trans_date_only 
        BETWEEN date_format(now(), '%Y-%m-11')-interval 1 month and curdate()-interval 1 month
        and left (bill_type,7) = 'PLN Pos' 
        and ((a.payment_status = 'RECEIVED' and a.order_status = 'SUCCESS')
                                or (a.payment_status = 'REFUND' and a.order_status = 'FAILED'))) as 'postpaid2'

        
        

        
        from IPEX_TRANS_MASTER a inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID limit 1");
        return $query->result_array();
    }


    function PLN_lastmonth(){
        $CI = &get_instance();
        $this->db2 = $CI->load->database('ipex', TRUE);
        $query = $this->db2->query("SELECT

        (select count(IPEX_TRANS_ID) from IPEX_TRANS_MASTER a 
        inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID 
        where trans_date_only 
        BETWEEN date_format(now(), '%Y-%m-01')-interval 1 month and date_format(now(), '%Y-%m-10')-interval 1 month
        and left (bill_type,7) = 'PLN Pre' 
        and ((a.payment_status = 'RECEIVED' and a.order_status = 'SUCCESS')
                                or (a.payment_status = 'REFUND' and a.order_status = 'FAILED'))) as 'prepaid1',
        
        (select count(IPEX_TRANS_ID) from IPEX_TRANS_MASTER a 
        inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID 
        where trans_date_only 
        BETWEEN date_format(now(), '%Y-%m-11')-interval 1 month and date_format(now(), '%Y-%m-20')-interval 1 month
        and left (bill_type,7) = 'PLN Pre' 
        and ((a.payment_status = 'RECEIVED' and a.order_status = 'SUCCESS')
                                or (a.payment_status = 'REFUND' and a.order_status = 'FAILED'))) as 'prepaid2',
        
        (select count(IPEX_TRANS_ID) from IPEX_TRANS_MASTER a 
        inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID 
        where trans_date_only 
        BETWEEN date_format(now(), '%Y-%m-21')-interval 1 month and last_day(now())-interval 1 month
        and left (bill_type,7) = 'PLN Pre' 
        and ((a.payment_status = 'RECEIVED' and a.order_status = 'SUCCESS')
                                or (a.payment_status = 'REFUND' and a.order_status = 'FAILED')))
         as 'prepaid3',
         
         (select count(IPEX_TRANS_ID) from IPEX_TRANS_MASTER a 
        inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID 
        where trans_date_only 
        BETWEEN date_format(now(), '%Y-%m-01')-interval 1 month and date_format(now(), '%Y-%m-10')-interval 1 month
        and left (bill_type,7) = 'PLN Pos' 
        and ((a.payment_status = 'RECEIVED' and a.order_status = 'SUCCESS')
                                or (a.payment_status = 'REFUND' and a.order_status = 'FAILED'))) as 'postpaid1',
        
        (select count(IPEX_TRANS_ID) from IPEX_TRANS_MASTER a 
        inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID 
        where trans_date_only 
        BETWEEN date_format(now(), '%Y-%m-11')-interval 1 month and date_format(now(), '%Y-%m-20')-interval 1 month
        and left (bill_type,7) = 'PLN Pos' 
        and ((a.payment_status = 'RECEIVED' and a.order_status = 'SUCCESS')
                                or (a.payment_status = 'REFUND' and a.order_status = 'FAILED'))) as 'postpaid2',
        
        (select count(IPEX_TRANS_ID) from IPEX_TRANS_MASTER a 
        inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID 
        where trans_date_only 
        BETWEEN date_format(now(), '%Y-%m-21')-interval 1 month and last_day(now()) -interval 1 month
        and left (bill_type,7) = 'PLN Pos' 
        and ((a.payment_status = 'RECEIVED' and a.order_status = 'SUCCESS')
                                or (a.payment_status = 'REFUND' and a.order_status = 'FAILED')))
         as 'postpaid3'
        
        

        
        from IPEX_TRANS_MASTER a inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID limit 1");
        return $query->result_array();
    }


    function PLN_thismonth(){
        $CI = &get_instance();
        $this->db2 = $CI->load->database('ipex', TRUE);
        // $query=$this->db2->query("select count(IPEX_TRANS_ID) as '$thisalias' from IPEX_TRANS_MASTER a 
        // inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID 
        // where TRANS_DATE_ONLY between date_format(now(),'%Y-%m-01')and curdate() and bill_type = '$bill_type'");
        $query=$this->db2->query("SELECT

        (select count(IPEX_TRANS_ID) from IPEX_TRANS_MASTER a 
        inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID 
        where trans_date_only 
        BETWEEN date_format(now(), '%Y-%m-01') and date_format(now(), '%Y-%m-10')
        and left (bill_type,7) = 'PLN Pre' 
        and ((a.payment_status = 'RECEIVED' and a.order_status = 'SUCCESS')
                                or (a.payment_status = 'REFUND' and a.order_status = 'FAILED'))) as 'prepaid1',
        
        (select count(IPEX_TRANS_ID) from IPEX_TRANS_MASTER a 
        inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID 
        where trans_date_only 
        BETWEEN date_format(now(), '%Y-%m-11') and date_format(now(), '%Y-%m-20')
        and left (bill_type,7) = 'PLN Pre' 
        and ((a.payment_status = 'RECEIVED' and a.order_status = 'SUCCESS')
                                or (a.payment_status = 'REFUND' and a.order_status = 'FAILED'))) as 'prepaid2',
        
        (select count(IPEX_TRANS_ID) from IPEX_TRANS_MASTER a 
        inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID 
        where trans_date_only 
        BETWEEN date_format(now(), '%Y-%m-21') and last_day(now())
        and left (bill_type,7) = 'PLN Pre' 
        and ((a.payment_status = 'RECEIVED' and a.order_status = 'SUCCESS')
                                or (a.payment_status = 'REFUND' and a.order_status = 'FAILED')))
         as 'prepaid3',
         
         (select count(IPEX_TRANS_ID) from IPEX_TRANS_MASTER a 
        inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID 
        where trans_date_only 
        BETWEEN date_format(now(), '%Y-%m-01') and date_format(now(), '%Y-%m-10')
        and left (bill_type,7) = 'PLN Pos' 
        and ((a.payment_status = 'RECEIVED' and a.order_status = 'SUCCESS')
                                or (a.payment_status = 'REFUND' and a.order_status = 'FAILED'))) as 'postpaid1',
        
        (select count(IPEX_TRANS_ID) from IPEX_TRANS_MASTER a 
        inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID 
        where trans_date_only 
        BETWEEN date_format(now(), '%Y-%m-11') and date_format(now(), '%Y-%m-20')
        and left (bill_type,7) = 'PLN Pos' 
        and ((a.payment_status = 'RECEIVED' and a.order_status = 'SUCCESS')
                                or (a.payment_status = 'REFUND' and a.order_status = 'FAILED'))) as 'postpaid2',
        
        (select count(IPEX_TRANS_ID) from IPEX_TRANS_MASTER a 
        inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID 
        where trans_date_only 
        BETWEEN date_format(now(), '%Y-%m-21') and last_day(now()) 
        and left (bill_type,7) = 'PLN Pos' 
        and ((a.payment_status = 'RECEIVED' and a.order_status = 'SUCCESS')
                                or (a.payment_status = 'REFUND' and a.order_status = 'FAILED')))
         as 'postpaid3'
        
        

        
        from IPEX_TRANS_MASTER a inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID limit 1");
        return $query->result_array();
    }

    //PDAM
    function get_PDAM()
    {
        $CI = &get_instance();
        $this->db2 = $CI->load->database('ipex', TRUE);
        $query=$this->db2->query("SELECT
        (select count(IPEX_TRANS_ID) from IPEX_TRANS_MASTER a inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID where trans_date_only = curdate() and left (bill_type,4) = 'PDAM' and ((payment_status = 'RECEIVED' and order_status = 'SUCCESS') or (payment_status = 'REFUND' and order_status = 'FAILED'))) as 'pdamtotal',
        (select count(IPEX_TRANS_ID) from IPEX_TRANS_MASTER a inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID where trans_date_only = curdate() and left (bill_type,4) = 'PDAM' and order_status = 'SUCCESS' and payment_status = 'RECEIVED') as 'pdamsukses',
        (select count(IPEX_TRANS_ID) from IPEX_TRANS_MASTER a inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID where trans_date_only = curdate() and left (bill_type,4) = 'PDAM' and order_status = 'FAILED' and payment_status = 'REFUND') as 'pdamgagal'
        
        from IPEX_TRANS_MASTER a inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID limit 1");
        return $query->result_array();
    }

    function PDAM_lastmonth(){
        $CI = &get_instance();
        $this->db2 = $CI->load->database('ipex', TRUE);
        $query=$this->db2->query("SELECT
                            (select count(IPEX_TRANS_ID) from IPEX_TRANS_MASTER a 
                                inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID 
                                where trans_date_only between date_format(now(), '%Y-%m-01')-interval 1 month and curdate()-interval 1 month and left (bill_type,4) = 'PDAM' 
                                and ((payment_status = 'RECEIVED' and order_status = 'SUCCESS') 
                                or (payment_status = 'REFUND' and order_status = 'FAILED'))
                                ) as 'pdamtotal',
                            (select count(IPEX_TRANS_ID) from IPEX_TRANS_MASTER a 
                                inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID 
                                where trans_date_only between date_format(now(), '%Y-%m-01')-interval 1 month and curdate()-interval 1 month and left (bill_type,4) = 'PDAM' 
                                and order_status = 'SUCCESS' and payment_status = 'RECEIVED'
                                ) as 'pdamsukses',
                            (select count(IPEX_TRANS_ID) from IPEX_TRANS_MASTER a 
                                inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID 
                                where trans_date_only between date_format(now(), '%Y-%m-01')-interval 1 month and curdate()-interval 1 month and left (bill_type,4) = 'PDAM' 
                                and order_status = 'FAILED' and payment_status = 'REFUND'
                                ) as 'pdamgagal'
                        from IPEX_TRANS_MASTER a 
                        inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID 
                        limit 1");
        return $query->result_array();
    }

    function PDAM_thismonth(){
        $CI = &get_instance();
        $this->db2 = $CI->load->database('ipex',TRUE);
        $query=$this->db2->query("SELECT
                        (select count(IPEX_TRANS_ID) from IPEX_TRANS_MASTER a 
                            inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID 
                            where trans_date_only between date_format(now(), '%Y-%m-01') and curdate() and left (bill_type,4) = 'PDAM' 
                            and ((payment_status = 'RECEIVED' and order_status = 'SUCCESS') 
                            or (payment_status = 'REFUND' and order_status = 'FAILED'))
                            ) as 'pdamtotal',
                        (select count(IPEX_TRANS_ID) from IPEX_TRANS_MASTER a 
                            inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID 
                            where trans_date_only between date_format(now(), '%Y-%m-01') and curdate() and left (bill_type,4) = 'PDAM' 
                            and order_status = 'SUCCESS' and payment_status = 'RECEIVED'
                            ) as 'pdamsukses',
                        (select count(IPEX_TRANS_ID) from IPEX_TRANS_MASTER a 
                            inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID 
                            where trans_date_only between date_format(now(), '%Y-%m-01') and curdate() and left (bill_type,4) = 'PDAM' 
                            and order_status = 'FAILED' and payment_status = 'REFUND'
                            ) as 'pdamgagal'
                    from IPEX_TRANS_MASTER a 
                    inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID 
                    limit 1");
        return $query->result_array();
    }
	// Operator
    function get_nominal_provider($value)
    {
        $CI = &get_instance();
        $this->db2 = $CI->load->database('ipex', TRUE);
        $query = $this->db2->query("select amount , operator_name , OPERATOR_REFERENCE_ID from IPEX_MOBILE_SERV_TRANS_DETAILS WHERE date(trans_date) = DATE(NOW()) and operator_name = '$value'");
        return $query->result_array();
    }
	
}



// select count(a.IPEX_TRANS_ID) as 'total prepaid' from IPEX_TRANS_MASTER a 
// inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID
// where b.BILL_TYPE = 'PLN Prepaid'
//             and ((a.payment_status = 'RECEIVED' and a.order_status = 'SUCCESS')
//             or (a.payment_status = 'REFUND' and a.order_status = 'FAILED'))
//             and a.TRANS_DATE_ONLY between date_format(now(),'%Y-%m-01') -interval 1 month 
//     and curdate()- INTERVAL 1 month

// select count(a.IPEX_TRANS_ID) as 'total postpaid' from IPEX_TRANS_MASTER a 
// inner join IPEX_BILLPAYMENT_SERV_TRANS_DETAILS b on a.IPEX_ORDER_ID = b.IPEX_ORDER_ID
// where b.bill_type = 'PLN Postpaid' and
//         ((a.payment_status = 'RECEIVED' and a.order_status = 'SUCCESS') 
//         or (a.payment_status = 'REFUND' and a.order_status = 'FAILED'))
//             and a.TRANS_DATE_ONLY between date_format(now(),'%Y-%m-01') -interval 1 month 
//     and curdate()- INTERVAL 1 month