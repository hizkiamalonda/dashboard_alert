<?php
class Totalkcp extends CI_Model{

    function get_class_this_month()
    {   
        $query = $this->db->query("SELECT CLASS, COUNT(CLASS) TOTAL FROM
            (SELECT retailer_id, CASE
                WHEN COUNT(order_id) BETWEEN 1 AND 10 THEN 'CLASS A'
                WHEN COUNT(order_id) BETWEEN 11 AND 50 THEN 'CLASS B'
                WHEN COUNT(order_id) BETWEEN 51 AND 100 THEN 'CLASS C'
                WHEN COUNT(order_id) > 101 THEN 'CLASS D'
                WHEN COUNT(order_id) = 0 THEN 'CLASS E'
                END AS CLASS
            FROM
                ipay_transactions
            WHERE
                payment_status = 'RECEIVED'
                AND order_status = 'SUCCESS'
                AND ISNULL(cancelled_reference_id)
                AND transaction_date_only BETWEEN DATE_FORMAT(NOW(), '%Y-%m-01') AND curdate()
            GROUP BY retailer_id) aaa
            GROUP BY CLASS ");

        return $query->result_array();
    }

    function get_class_last_month()
    {   
        $query = $this->db->query("SELECT CLASS, COUNT(CLASS) TOTAL FROM
            (SELECT retailer_id, CASE
                WHEN COUNT(order_id) BETWEEN 1 AND 10 THEN 'CLASS A'
                WHEN COUNT(order_id) BETWEEN 11 AND 50 THEN 'CLASS B'
                WHEN COUNT(order_id) BETWEEN 51 AND 100 THEN 'CLASS C'
                WHEN COUNT(order_id) > 101 THEN 'CLASS D'
                WHEN COUNT(order_id) = 0 THEN 'CLASS E'
                END AS CLASS
            FROM
                ipay_transactions
            WHERE
                payment_status = 'RECEIVED'
                AND order_status = 'SUCCESS'
                AND ISNULL(cancelled_reference_id)
                AND transaction_date_only BETWEEN DATE_FORMAT(now(), '%Y-%m-01') - interval 1 month 
                AND curdate()-interval 1 month
            GROUP BY retailer_id) aaa
            GROUP BY CLASS ");

        return $query->result_array();
    }

    function get_class_this_day()
    {   
        $query = $this->db->query("SELECT CLASS, COUNT(CLASS) TOTAL FROM
            (SELECT retailer_id, CASE
                WHEN COUNT(order_id) BETWEEN 1 AND 10 THEN 'CLASS A'
                WHEN COUNT(order_id) BETWEEN 11 AND 50 THEN 'CLASS B'
                WHEN COUNT(order_id) BETWEEN 51 AND 100 THEN 'CLASS C'
                WHEN COUNT(order_id) > 101 THEN 'CLASS D'
                WHEN COUNT(order_id) = 0 THEN 'CLASS E'
                END AS CLASS
            FROM
                ipay_transactions
            WHERE
                payment_status = 'RECEIVED'
                AND order_status = 'SUCCESS'
                AND ISNULL(cancelled_reference_id)
                AND transaction_date_only = curdate()
            GROUP BY retailer_id) aaa
            GROUP BY CLASS ");

        return $query->result_array();
    }

}













