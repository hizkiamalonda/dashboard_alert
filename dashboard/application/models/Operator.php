<?php
class Operator extends CI_Model{

	// function get_operator_status($operator){ //today
 //        $CI = &get_instance();
 //        $this->db2 = $CI->load->database('ipex',TRUE);
 //        $query = $this->db2->query("SELECT td.PRODUCT_CODE,
 //                                COUNT(
 //                                    IF(tm.PAYMENT_STATUS = 'RECEIVED'
 //                                        AND tm.ORDER_STATUS = 'SUCCESS'
 //                                        ,1,NULL)
 //                                    ) AS SUCCESS,
 //                                COUNT(
 //                                    IF(tm.PAYMENT_STATUS = 'REFUND'
 //                                        AND tm.ORDER_STATUS = 'FAILED'
 //                                        ,1,NULL)
 //                                    ) AS FAILED
 //                            FROM IPEX_TRANS_MASTER tm
 //                            inner join IPEX_MOBILE_SERV_TRANS_DETAILS td on tm.IPEX_ORDER_ID = td.IPEX_ORDER_ID
 //                            where td.OPERATOR_NAME = '$operator' and tm.TRANS_DATE_ONLY = curdate()
 //                            group by td.PRODUCT_CODE
 //                            order by AMOUNT");
 //        // die(var_dump($query));
 //        return $query->result_array();
 //    }
    function get_operator_status(){ //today
        $CI = &get_instance();
        $this->db2 = $CI->load->database('ipex',TRUE);
        $query = $this->db2->query("SELECT td.PRODUCT_CODE,OPERATOR_NAME,
                                COUNT(
                                    IF(tm.PAYMENT_STATUS = 'RECEIVED'
                                        AND tm.ORDER_STATUS = 'SUCCESS'
                                        ,1,NULL)
                                    ) AS SUCCESS,
                                COUNT(
                                    IF(tm.PAYMENT_STATUS = 'REFUND'
                                        AND tm.ORDER_STATUS = 'FAILED'
                                        ,1,NULL)
                                    ) AS FAILED
                            FROM IPEX_TRANS_MASTER tm
                            inner join IPEX_MOBILE_SERV_TRANS_DETAILS td on tm.IPEX_ORDER_ID = td.IPEX_ORDER_ID
                            where tm.TRANS_DATE_ONLY = curdate()
                            and left(td.PRODUCT_CODE,2) = 'MR'
                            and td.VENDOR_TRANS_ID != 'null' 
                            group by td.PRODUCT_CODE
                            order by OPERATOR_NAME,AMOUNT");
        // die(var_dump($query));
        return $query->result_array();
    }

    function get_operator_status_lastmonth(){
        $CI = &get_instance();
        $this->db2 = $CI->load->database('ipex',TRUE);
        $query = $this->db2->query("SELECT td.PRODUCT_CODE, OPERATOR_NAME,
                                        COUNT(
                                            IF(tm.PAYMENT_STATUS = 'RECEIVED'
                                                AND tm.ORDER_STATUS = 'SUCCESS'
                                                ,1,NULL)
                                            ) AS SUCCESS,
                                        COUNT(
                                            IF(tm.PAYMENT_STATUS = 'REFUND'
                                                AND tm.ORDER_STATUS = 'FAILED'
                                                ,1,NULL)
                                            ) AS FAILED
                                    FROM IPEX_TRANS_MASTER tm
                                    inner join IPEX_MOBILE_SERV_TRANS_DETAILS td on tm.IPEX_ORDER_ID = td.IPEX_ORDER_ID
                                    where  TRANS_DATE_ONLY between DATE_FORMAT(curdate(), '%Y-%m-01') - interval 1 month 
                                    and curdate()-interval 1 month
                                    and left(td.PRODUCT_CODE,2) = 'MR'
                                    and td.VENDOR_TRANS_ID != 'null' 
                                    group by td.PRODUCT_CODE
                                    order by OPERATOR_NAME, AMOUNT");
        return $query->result_array();
    }

    function get_operator_status_curmonth(){
        $CI = &get_instance();
        $this->db2 = $CI->load->database('ipex',TRUE);
        $query = $this->db2->query("SELECT td.PRODUCT_CODE,OPERATOR_NAME,
                                        COUNT(
                                            IF(tm.PAYMENT_STATUS = 'RECEIVED'
                                                AND tm.ORDER_STATUS = 'SUCCESS'
                                                ,1,NULL)
                                            ) AS SUCCESS,
                                        COUNT(
                                            IF(tm.PAYMENT_STATUS = 'REFUND'
                                                AND tm.ORDER_STATUS = 'FAILED'
                                                ,1,NULL)
                                            ) AS FAILED
                                    FROM IPEX_TRANS_MASTER tm
                                    inner join IPEX_MOBILE_SERV_TRANS_DETAILS td on tm.IPEX_ORDER_ID = td.IPEX_ORDER_ID
                                    where TRANS_DATE_ONLY between DATE_FORMAT(curdate(), '%Y-%m-01') 
                                    and curdate()
                                    and left(td.PRODUCT_CODE,2) = 'MR'
                                    and td.VENDOR_TRANS_ID != 'null' 
                                    group by td.PRODUCT_CODE
                                    order by OPERATOR_NAME,AMOUNT");
        return $query->result_array();
    }

    function get_product_code($operator){
        
        $query = $this->db->query("SELECT talktime,description from ipay_popular_recharges where operator = '$operator' and left(talktime,2)='MR' and circle = 'JK' order by Amount");
        return $query->result_array();
    }

    // function get_product_code(){
        
    //     $query = $this->db->query("SELECT talktime,description, operator
    //                         from ipay_popular_recharges 
    //                         where left(talktime,2)='MR' 
    //                         and circle = 'JK' order by operator,Amount");
    //     return $query->result_array();
    // }


}
