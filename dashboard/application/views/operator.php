		<style>

			.table tr th{
				text-align: center;
				color:white;
			}
			.table tr td{
				color:white;
				text-align: center;
			}
			.table tbody tr td{
				border-top: 0px!important;
				line-height: 0.828571!important;
			}

			.table tbody tr th{
				border-top: 0px!important;
				border-bottom: 1px solid white!important;
			}
			@media only screen and (max-width: 991px){
				.op-today {
					left: -25%!important;
					padding-top: 2%!important;
				}
				.op-margin {
					margin-left: 0%!important;
				}
			}
			@media only screen and (max-width: 767px){
				.op-today {
					left: 0%!important;
					padding-top: 2%!important;
				}
			}

		</style>
			<!-- Operator -->
			<div id="op">
				<!-- Baris 1 -->
				<div class="col-12 row" style="text-align: center; margin: 0">	<!-- class=".tile_waris1" -->
					
					
					<!-- TELKOMSEL -->
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 5%; ">
						<!-- LOGO TELKOMSEL -->
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: white; width:100%; height:20%; padding: 0% 0% 0% 1%;border-top-left-radius: 10px; border-top-right-radius: 10px">
							<img class="" src="<?php echo base_url()."/assets/";?>build/img/telkomsel.jpg" alt="telkomsel_icon" height="100%">
						</div>
						<!-- TELKOMSEL LAST MONTH -->
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="padding: 0% 0% 0 0%">	
							<!-- TRANSAKSI TOTAL -->
							<div style="background-color: white; padding: 2% 0 3% 0; border-top-left-radius: 0px; border-top-right-radius: 0px">
								<h5 class="count_top" style="color: gray"><b>LtD</b></h5>
								<div class="count">
									<h1 style="font-weight: bold; color: black; margin-bottom:0px">
										<?php
											$sum=0;
											foreach ($operator_status_lastmtd as $key => $value) {
												if($value['OPERATOR_NAME'] == 'TELKOMSEL'){
													$sum += $value['SUCCESS']+$value['FAILED'];
												}
											}
											echo number_format($sum);
										?>
									</h1>
								</div>
							</div>
							<!-- SUCCES FAILED PERCENT		 -->
							<div style="background-color: #eaf1fc; padding: 5% 0 5% 0">
								<div class="container-fluid">
									<div class="col-12 row" style="text-align: center">
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
											<h6 style="font-weight: bold">SUCCESS</h6>
											<h6>
												<?php 
													$sum=0;
													foreach ($operator_status_lastmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'TELKOMSEL'){
															$sum += $value['SUCCESS'];
														}
													}
													echo number_format($sum);
												?>
											</h6>
										</div>
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
											<h6 style="font-weight: bold">FAILED</h6>
											<h6>
												<?php 
													$sum=0;
													foreach ($operator_status_lastmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'TELKOMSEL'){
															$sum += $value['FAILED'];
														}
													}
													echo number_format($sum); 
												?>
											</h6>
										</div>
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
											<h6 style="font-weight: bold">%</h6>
											<h6>
												<?php
													$percent = 0; $sum_s=0; $sum_f=0;
													foreach ($operator_status_lastmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'TELKOMSEL'){
															$sum_s+=$value['SUCCESS'];
															$sum_f+=$value['FAILED']; 
														}
													}
													if(($sum_s+$sum_f) == 0){
														echo 0;
													}else{
														$percent = ($sum_s/($sum_s+$sum_f))*100;
														echo number_format($percent);	
													}
												?>
											</h6>
										</div>
									</div>
								</div>
							</div>
							<!-- LIST OF NOMINAL -->
							<div style="background-color: #54b1f9; padding: 2% 0 5% 0; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px">
								<div class="container-fluid">
									<div class="col-12 row" style="text-align: center">
										<table class="table" style="color:white;text-align:center;" >
											<tr>
												<th>Nominal</th>
												<th>Success</th>
												<th>Failed</th>
												<th>%</th>
											</tr>

										<?php for ($i=0; $i <sizeof($telkomsel_code) ; $i++) { ?> 
											<tr> 
												
												<td><?php //nominal
													$temp=explode(" ",$telkomsel_code[$i]['description']);
													echo $temp[sizeof($temp)-1];	
											    	?></td>
												<td><?php $success = 0; //success
													foreach ($operator_status_lastmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'TELKOMSEL'){
															if($value['PRODUCT_CODE']==$telkomsel_code[$i]['talktime']){
																$success = $value['SUCCESS'];
																break;
															}
														}
													}
													echo number_format($success);
													?></td>
												<td><?php $failed = 0; //failed
													foreach ($operator_status_lastmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'TELKOMSEL'){
															if($value['PRODUCT_CODE']==$telkomsel_code[$i]['talktime']){
																$failed = $value['FAILED'];
																break;
															}
														}
													}
													echo number_format($failed);
														?></td>
												<td><?php $percent = 0; //percent
													foreach ($operator_status_lastmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'TELKOMSEL'){
															if($value['PRODUCT_CODE']==$telkomsel_code[$i]['talktime']){
																$percent = ($value['SUCCESS']/($value['SUCCESS']+$value['FAILED']))*100;
																break;
															}
														}
													}
													if($percent>=0){
														echo number_format($percent);
													}else{
														echo 0;
													}
													?></td>
											</tr>
										 <?php } ?>	
										</table>
									</div>
								</div>
							</div>
						</div>
						<!-- TELKOMSEL CURRENT MONTH -->
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="padding: 0% 0% 0 1%">
							<!-- TRANSAKSI TOTAL -->
							<div class="op-margin" style="background-color: white; padding: 2% 0 3% 0%; margin-left: -4%;">
								<h5 class="count_top" style="color: gray"><b>MtD</b></h5>
								<div class="count">
									<h1 style="font-weight: bold; color: black; margin-bottom:0px">
										<?php
											$sum=0;
											foreach ($operator_status_curmtd as $key => $value) {
												if($value['OPERATOR_NAME'] == 'TELKOMSEL'){
													$sum += $value['SUCCESS']+$value['FAILED'];
												}
											}
											echo number_format($sum);
										?>
									</h1>
								</div>
							</div>
							<!-- SUCCES FAILED PERCENT		 -->
							<div style="background-color: #eaf1fc; padding: 5% 0 5% 0">
								<div class="container-fluid">
									<div class="col-12 row" style="text-align: center">
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
											<h6 style="font-weight: bold">SUCCESS</h6>
											<h6>
												<?php 
													$sum=0;
													foreach ($operator_status_curmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'TELKOMSEL'){
															$sum += $value['SUCCESS'];
														}
													}
													echo number_format($sum);
												?>
											</h6>
										</div>
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
											<h6 style="font-weight: bold">FAILED</h6>
											<h6>
												<?php 
													$sum=0;
													foreach ($operator_status_curmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'TELKOMSEL'){
															$sum += $value['FAILED'];
														}
													}
													echo number_format($sum); 
												?>
											</h6>
										</div>
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
											<h6 style="font-weight: bold">%</h6>
											<h6>
												<?php
													$percent = 0; $sum_s=0; $sum_f=0;
													foreach ($operator_status_curmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'TELKOMSEL'){
															$sum_s+=$value['SUCCESS'];
															$sum_f+=$value['FAILED']; 
														}
													}
													if(($sum_s+$sum_f) == 0){
														echo 0;
													}else{
														$percent = ($sum_s/($sum_s+$sum_f))*100;
														echo number_format($percent);	
													}
												?>
											</h6>
										</div>
									</div>
								</div>
							</div>
							<!-- LIST OF NOMINAL -->
							<div style="background-color: #54b1f9; padding: 2% 0 5% 0; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px">
								<div class="container-fluid">
									<div class="col-12 row" style="text-align: center">
										<table class="table" style="color:white;text-align:center;" >
											<tr>
												<th>Nominal</th>
												<th>Success</th>
												<th>Failed</th>
												<th>%</th>
											</tr>

										<?php for ($i=0; $i <sizeof($telkomsel_code) ; $i++) { ?> 
											<tr> 
												
												<td><?php //nominal
													$temp=explode(" ",$telkomsel_code[$i]['description']);
													echo $temp[sizeof($temp)-1];	
											    	?></td>
												<td><?php $success = 0; //success
													foreach ($operator_status_curmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'TELKOMSEL'){
															if($value['PRODUCT_CODE']==$telkomsel_code[$i]['talktime']){
																$success = $value['SUCCESS'];
																break;
															}
														}
													}
													echo number_format($success);
													?></td>
												<td><?php $failed = 0; //failed
													foreach ($operator_status_curmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'TELKOMSEL'){
															if($value['PRODUCT_CODE']==$telkomsel_code[$i]['talktime']){
																$failed = $value['FAILED'];
																break;
															}
														}
													}
													echo number_format($failed);
														?></td>
												<td><?php $percent = 0; //percent
													foreach ($operator_status_curmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'TELKOMSEL'){
															if($value['PRODUCT_CODE']==$telkomsel_code[$i]['talktime']){
																$percent = ($value['SUCCESS']/($value['SUCCESS']+$value['FAILED']))*100;
																break;
															}
														}
													}
													if($percent>=0){
														echo number_format($percent);
													}else{
														echo 0;
													}
													?></td>
											</tr>
										 <?php } ?>	
										</table>
									</div>
								</div>
							</div>
						</div>
						<!-- TELKOMSEL TODAY -->
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 op-today" style="padding: 0% 0% 0 1%">	
							<!-- TRANSAKSI TOTAL -->
							<div class="op-margin" style="background-color: white; padding: 2% 0 3% 0; margin-left: -4%;">
								<h5 class="count_top" style="color: gray"><b>Today</b></h5>
								<div class="count">
									<h1 style="font-weight: bold; color: black; margin-bottom:0px">
										<?php
											$sum=0;
											foreach ($operator_status as $key => $value) {
												if($value['OPERATOR_NAME'] == 'TELKOMSEL'){
													$sum += $value['SUCCESS']+$value['FAILED'];
												}
											}
											echo number_format($sum);
										?>
									</h1>
								</div>
							</div>
							<!-- SUCCES FAILED PERCENT		 -->
							<div style="background-color: #eaf1fc; padding: 5% 0 5% 0">
								<div class="container-fluid">
									<div class="col-12 row" style="text-align: center">
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
											<h6 style="font-weight: bold">SUCCESS</h6>
											<h6>
												<?php 
													$sum=0;
													foreach ($operator_status as $key => $value) {
														if($value['OPERATOR_NAME'] == 'TELKOMSEL'){
															$sum += $value['SUCCESS'];
														}
													}
													echo number_format($sum);
												?>
											</h6>
										</div>
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
											<h6 style="font-weight: bold">FAILED</h6>
											<h6>
												<?php 
													$sum=0;
													foreach ($operator_status as $key => $value) {
														if($value['OPERATOR_NAME'] == 'TELKOMSEL'){
															$sum += $value['FAILED'];
														}
													}
													echo number_format($sum); 
												?>
											</h6>
										</div>
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
											<h6 style="font-weight: bold">%</h6>
											<h6>
												<?php
													$percent = 0; $sum_s=0; $sum_f=0;
													foreach ($operator_status as $key => $value) {
														if($value['OPERATOR_NAME'] == 'TELKOMSEL'){
															$sum_s+=$value['SUCCESS'];
															$sum_f+=$value['FAILED']; 
														}
													}
													if(($sum_s+$sum_f) == 0){
														echo 0;
													}else{
														$percent = ($sum_s/($sum_s+$sum_f))*100;
														echo number_format($percent);	
													}
												?>
											</h6>
										</div>
									</div>
								</div>
							</div>
							<!-- LIST OF NOMINAL -->
							<div style="background-color: #54b1f9; padding: 2% 0 5% 0; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px">
								<div class="container-fluid">
									<div class="col-12 row" style="text-align: center">
										<table class="table" style="color:white;text-align:center;" >
											<tr>
												<th>Nominal</th>
												<th>Success</th>
												<th>Failed</th>
												<th>%</th>
											</tr>

										<?php for ($i=0; $i <sizeof($telkomsel_code) ; $i++) { ?> 
											<tr> 
												
												<td><?php //nominal
													$temp=explode(" ",$telkomsel_code[$i]['description']);
													echo $temp[sizeof($temp)-1];	
											    	?></td>
												<td><?php $success = 0; //success
													foreach ($operator_status as $key => $value) {
														if($value['OPERATOR_NAME'] == 'TELKOMSEL'){
															if($value['PRODUCT_CODE']==$telkomsel_code[$i]['talktime']){
																$success = $value['SUCCESS'];
																break;
															}
														}
													}
													echo number_format($success);
													?></td>
												<td><?php $failed = 0; //failed
													foreach ($operator_status as $key => $value) {
														if($value['OPERATOR_NAME'] == 'TELKOMSEL'){
															if($value['PRODUCT_CODE']==$telkomsel_code[$i]['talktime']){
																$failed = $value['FAILED'];
																break;
															}
														}
													}
													echo number_format($failed);
														?></td>
												<td><?php $percent = 0; //percent
													foreach ($operator_status as $key => $value) {
														if($value['OPERATOR_NAME'] == 'TELKOMSEL'){
															if($value['PRODUCT_CODE']==$telkomsel_code[$i]['talktime']){
																$percent = ($value['SUCCESS']/($value['SUCCESS']+$value['FAILED']))*100;
																break;
															}
														}
													}
													if($percent>=0){
														echo number_format($percent);
													}else{
														echo 0;
													}
													?></td>
											</tr>
										 <?php } ?>	
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- INDOSAT -->
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 5%; ">
						<!-- LOGO INDOSAT -->
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: white; width:100%; height:15%; padding: 0% 0% 0% 1%;border-top-left-radius: 10px; border-top-right-radius: 10px">
							<img class="" src="<?php echo base_url()."/assets/";?>build/img/indosat.jpg" alt="telkomsel_icon" height="100%">
						</div>
						<!-- INDOSAT LAST MONTH -->
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="padding: 0% 0% 0 0%">	
							<!-- TRANSAKSI TOTAL -->
							<div style="background-color: white; padding: 2% 0 3% 0; border-top-left-radius: 0px; border-top-right-radius: 0px">
								<h5 class="count_top" style="color: gray"><b>LtD</b></h5>
								<div class="count">
									<h1 style="font-weight: bold; color: black; margin-bottom:0px">
										<?php
											$sum=0;
											foreach ($operator_status_lastmtd as $key => $value) {
												if($value['OPERATOR_NAME'] == 'INDOSAT'){
													$sum += $value['SUCCESS']+$value['FAILED'];
												}
											}
											echo number_format($sum);
										?>
									</h1>
								</div>
							</div>
							<!-- SUCCES FAILED PERCENT		 -->
							<div style="background-color: #eaf1fc; padding: 5% 0 5% 0">
								<div class="container-fluid">
									<div class="col-12 row" style="text-align: center">
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
											<h6 style="font-weight: bold">SUCCESS</h6>
											<h6>
												<?php 
													$sum=0;
													foreach ($operator_status_lastmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'INDOSAT'){
															$sum += $value['SUCCESS'];
														}
													}
													echo number_format($sum);
												?>
											</h6>
										</div>
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
											<h6 style="font-weight: bold">FAILED</h6>
											<h6>
												<?php 
													$sum=0;
													foreach ($operator_status_lastmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'INDOSAT'){
															$sum += $value['FAILED'];
														}
													}
													echo number_format($sum); 
												?>
											</h6>
										</div>
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
											<h6 style="font-weight: bold">%</h6>
											<h6>
												<?php
													$percent = 0; $sum_s=0; $sum_f=0;
													foreach ($operator_status_lastmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'INDOSAT'){
															$sum_s+=$value['SUCCESS'];
															$sum_f+=$value['FAILED']; 
														}
													}
													if(($sum_s+$sum_f) == 0){
														echo 0;
													}else{
														$percent = ($sum_s/($sum_s+$sum_f))*100;
														echo number_format($percent);	
													}
												?>
											</h6>
										</div>
									</div>
								</div>
							</div>
							<!-- LIST OF NOMINAL -->
							<div style="background-color: #54b1f9; padding: 2% 0 5% 0; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px">
								<div class="container-fluid">
									<div class="col-12 row" style="text-align: center">
										<table class="table" style="color:white;text-align:center;" >
											<tr>
												<th>Nominal</th>
												<th>Success</th>
												<th>Failed</th>
												<th>%</th>
											</tr>

										<?php for ($i=0; $i <sizeof($indosat_code) ; $i++) { ?> 
											<tr> 
												
												<td><?php //nominal
													$temp=explode(" ",$indosat_code[$i]['description']);
													echo $temp[sizeof($temp)-1];	
											    	?></td>
												<td><?php $success = 0; //success
													foreach ($operator_status_lastmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'INDOSAT'){
															if($value['PRODUCT_CODE']==$indosat_code[$i]['talktime']){
																$success = $value['SUCCESS'];
																break;
															}
														}
													}
													echo number_format($success);
													?></td>
												<td><?php $failed = 0; //failed
													foreach ($operator_status_lastmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'INDOSAT'){
															if($value['PRODUCT_CODE']==$indosat_code[$i]['talktime']){
																$failed = $value['FAILED'];
																break;
															}
														}
													}
													echo number_format($failed);
														?></td>
												<td><?php $percent = 0; //percent
													foreach ($operator_status_lastmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'INDOSAT'){
															if($value['PRODUCT_CODE']==$indosat_code[$i]['talktime']){
																$percent = ($value['SUCCESS']/($value['SUCCESS']+$value['FAILED']))*100;
																break;
															}
														}
													}
													if($percent>=0){
														echo number_format($percent);
													}else{
														echo 0;
													}
													?></td>
											</tr>
										 <?php } ?>	
										</table>
									</div>
								</div>
							</div>
						</div>
						<!-- INDOSAT CURRENT MONTH -->
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="padding: 0% 0% 0 1%">
							<!-- TRANSAKSI TOTAL -->
							<div class="op-margin" style="background-color: white; padding: 2% 0 3% 0%; margin-left: -4%;">
								<h5 class="count_top" style="color: gray"><b>MtD</b></h5>
								<div class="count">
									<h1 style="font-weight: bold; color: black; margin-bottom:0px">
										<?php
											$sum=0;
											foreach ($operator_status_curmtd as $key => $value) {
												if($value['OPERATOR_NAME'] == 'INDOSAT'){
													$sum += $value['SUCCESS']+$value['FAILED'];
												}
											}
											echo number_format($sum);
										?>
									</h1>
								</div>
							</div>
							<!-- SUCCES FAILED PERCENT		 -->
							<div style="background-color: #eaf1fc; padding: 5% 0 5% 0">
								<div class="container-fluid">
									<div class="col-12 row" style="text-align: center">
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
											<h6 style="font-weight: bold">SUCCESS</h6>
											<h6>
												<?php 
													$sum=0;
													foreach ($operator_status_curmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'INDOSAT'){
															$sum += $value['SUCCESS'];
														}
													}
													echo number_format($sum);
												?>
											</h6>
										</div>
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
											<h6 style="font-weight: bold">FAILED</h6>
											<h6>
												<?php 
													$sum=0;
													foreach ($operator_status_curmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'INDOSAT'){
															$sum += $value['FAILED'];
														}
													}
													echo number_format($sum); 
												?>
											</h6>
										</div>
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
											<h6 style="font-weight: bold">%</h6>
											<h6>
												<?php
													$percent = 0; $sum_s=0; $sum_f=0;
													foreach ($operator_status_curmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'INDOSAT'){
															$sum_s+=$value['SUCCESS'];
															$sum_f+=$value['FAILED']; 
														}
													}
													if(($sum_s+$sum_f) == 0){
														echo 0;
													}else{
														$percent = ($sum_s/($sum_s+$sum_f))*100;
														echo number_format($percent);	
													}
												?>
											</h6>
										</div>
									</div>
								</div>
							</div>
							<!-- LIST OF NOMINAL -->
							<div style="background-color: #54b1f9; padding: 2% 0 5% 0; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px">
								<div class="container-fluid">
									<div class="col-12 row" style="text-align: center">
										<table class="table" style="color:white;text-align:center;" >
											<tr>
												<th>Nominal</th>
												<th>Success</th>
												<th>Failed</th>
												<th>%</th>
											</tr>

										<?php for ($i=0; $i <sizeof($indosat_code) ; $i++) { ?> 
											<tr> 
												
												<td><?php //nominal
													$temp=explode(" ",$indosat_code[$i]['description']);
													echo $temp[sizeof($temp)-1];	
											    	?></td>
												<td><?php $success = 0; //success
													foreach ($operator_status_curmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'INDOSAT'){
															if($value['PRODUCT_CODE']==$indosat_code[$i]['talktime']){
																$success = $value['SUCCESS'];
																break;
															}
														}
													}
													echo number_format($success);
													?></td>
												<td><?php $failed = 0; //failed
													foreach ($operator_status_curmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'INDOSAT'){
															if($value['PRODUCT_CODE']==$indosat_code[$i]['talktime']){
																$failed = $value['FAILED'];
																break;
															}
														}
													}
													echo number_format($failed);
														?></td>
												<td><?php $percent = 0; //percent
													foreach ($operator_status_curmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'INDOSAT'){
															if($value['PRODUCT_CODE']==$indosat_code[$i]['talktime']){
																$percent = ($value['SUCCESS']/($value['SUCCESS']+$value['FAILED']))*100;
																break;
															}
														}
													}
													if($percent>=0){
														echo number_format($percent);
													}else{
														echo 0;
													}
													?></td>
											</tr>
										 <?php } ?>	
										</table>
									</div>
								</div>
							</div>
						</div>
						<!-- INDOSAT TODAY -->
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 op-today" style="padding: 0% 0% 0 1%">	
							<!-- TRANSAKSI TOTAL -->
							<div class="op-margin" style="background-color: white; padding: 2% 0 3% 0; margin-left: -4%;">
								<h5 class="count_top" style="color: gray"><b>Today</b></h5>
								<div class="count">
									<h1 style="font-weight: bold; color: black; margin-bottom:0px">
										<?php
											$sum=0;
											foreach ($operator_status as $key => $value) {
												if($value['OPERATOR_NAME'] == 'INDOSAT'){
													$sum += $value['SUCCESS']+$value['FAILED'];
												}
											}
											echo number_format($sum);
										?>
									</h1>
								</div>
							</div>
							<!-- SUCCES FAILED PERCENT		 -->
							<div style="background-color: #eaf1fc; padding: 5% 0 5% 0">
								<div class="container-fluid">
									<div class="col-12 row" style="text-align: center">
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
											<h6 style="font-weight: bold">SUCCESS</h6>
											<h6>
												<?php 
													$sum=0;
													foreach ($operator_status as $key => $value) {
														if($value['OPERATOR_NAME'] == 'INDOSAT'){
															$sum += $value['SUCCESS'];
														}
													}
													echo number_format($sum);
												?>
											</h6>
										</div>
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
											<h6 style="font-weight: bold">FAILED</h6>
											<h6>
												<?php 
													$sum=0;
													foreach ($operator_status as $key => $value) {
														if($value['OPERATOR_NAME'] == 'INDOSAT'){
															$sum += $value['FAILED'];
														}
													}
													echo number_format($sum); 
												?>
											</h6>
										</div>
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
											<h6 style="font-weight: bold">%</h6>
											<h6>
												<?php
													$percent = 0; $sum_s=0; $sum_f=0;
													foreach ($operator_status as $key => $value) {
														if($value['OPERATOR_NAME'] == 'INDOSAT'){
															$sum_s+=$value['SUCCESS'];
															$sum_f+=$value['FAILED']; 
														}
													}
													if(($sum_s+$sum_f) == 0){
														echo 0;
													}else{
														$percent = ($sum_s/($sum_s+$sum_f))*100;
														echo number_format($percent);	
													}
												?>
											</h6>
										</div>
									</div>
								</div>
							</div>
							<!-- LIST OF NOMINAL -->
							<div style="background-color: #54b1f9; padding: 2% 0 5% 0; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px">
								<div class="container-fluid">
									<div class="col-12 row" style="text-align: center">
										<table class="table" style="color:white;text-align:center;" >
											<tr>
												<th>Nominal</th>
												<th>Success</th>
												<th>Failed</th>
												<th>%</th>
											</tr>

										<?php for ($i=0; $i <sizeof($indosat_code) ; $i++) { ?> 
											<tr> 
												
												<td><?php //nominal
													$temp=explode(" ",$indosat_code[$i]['description']);
													echo $temp[sizeof($temp)-1];	
											    	?></td>
												<td><?php $success = 0; //success
													foreach ($operator_status as $key => $value) {
														if($value['OPERATOR_NAME'] == 'INDOSAT'){
															if($value['PRODUCT_CODE']==$indosat_code[$i]['talktime']){
																$success = $value['SUCCESS'];
																break;
															}
														}
													}
													echo number_format($success);
													?></td>
												<td><?php $failed = 0; //failed
													foreach ($operator_status as $key => $value) {
														if($value['OPERATOR_NAME'] == 'INDOSAT'){
															if($value['PRODUCT_CODE']==$indosat_code[$i]['talktime']){
																$failed = $value['FAILED'];
																break;
															}
														}
													}
													echo number_format($failed);
														?></td>
												<td><?php $percent = 0; //percent
													foreach ($operator_status as $key => $value) {
														if($value['OPERATOR_NAME'] == 'INDOSAT'){
															if($value['PRODUCT_CODE']==$indosat_code[$i]['talktime']){
																$percent = ($value['SUCCESS']/($value['SUCCESS']+$value['FAILED']))*100;
																break;
															}
														}
													}
													if($percent>=0){
														echo number_format($percent);
													}else{
														echo 0;
													}
													?></td>
											</tr>
										 <?php } ?>	
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- XL -->
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 5%; ">
						<!-- LOGO XL -->
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: white; width:100%; height:28%; padding: 0% 0% 0% 1%;border-top-left-radius: 10px; border-top-right-radius: 10px">
							<img class="" src="<?php echo base_url()."/assets/";?>build/img/XL.jpg" alt="telkomsel_icon" height="100%">
						</div>
						<!-- XL LAST MONTH -->
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="padding: 0% 0% 0 0%">	
							<!-- TRANSAKSI TOTAL -->
							<div style="background-color: white; padding: 2% 0 3% 0; border-top-left-radius: 0px; border-top-right-radius: 0px">
								<h5 class="count_top" style="color: gray"><b>LtD</b></h5>
								<div class="count">
									<h1 style="font-weight: bold; color: black; margin-bottom:0px">
										<?php
											$sum=0;
											foreach ($operator_status_lastmtd as $key => $value) {
												if($value['OPERATOR_NAME'] == 'XL'){
													$sum += $value['SUCCESS']+$value['FAILED'];
												}
											}
											echo number_format($sum);
										?>
									</h1>
								</div>
							</div>
							<!-- SUCCES FAILED PERCENT		 -->
							<div style="background-color: #eaf1fc; padding: 5% 0 5% 0">
								<div class="container-fluid">
									<div class="col-12 row" style="text-align: center">
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
											<h6 style="font-weight: bold">SUCCESS</h6>
											<h6>
												<?php 
													$sum=0;
													foreach ($operator_status_lastmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'XL'){
															$sum += $value['SUCCESS'];
														}
													}
													echo number_format($sum);
												?>
											</h6>
										</div>
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
											<h6 style="font-weight: bold">FAILED</h6>
											<h6>
												<?php 
													$sum=0;
													foreach ($operator_status_lastmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'XL'){
															$sum += $value['FAILED'];
														}
													}
													echo number_format($sum); 
												?>
											</h6>
										</div>
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
											<h6 style="font-weight: bold">%</h6>
											<h6>
												<?php
													$percent = 0; $sum_s=0; $sum_f=0;
													foreach ($operator_status_lastmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'XL'){
															$sum_s+=$value['SUCCESS'];
															$sum_f+=$value['FAILED']; 
														}
													}
													if(($sum_s+$sum_f) == 0){
														echo 0;
													}else{
														$percent = ($sum_s/($sum_s+$sum_f))*100;
														echo number_format($percent);	
													}
												?>
											</h6>
										</div>
									</div>
								</div>
							</div>
							<!-- LIST OF NOMINAL -->
							<div style="background-color: #54b1f9; padding: 2% 0 5% 0; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px">
								<div class="container-fluid">
									<div class="col-12 row" style="text-align: center">
										<table class="table" style="color:white;text-align:center;" >
											<tr>
												<th>Nominal</th>
												<th>Success</th>
												<th>Failed</th>
												<th>%</th>
											</tr>

										<?php for ($i=0; $i <sizeof($xl_code) ; $i++) { ?> 
											<tr> 
												
												<td><?php //nominal
													$temp=explode(" ",$xl_code[$i]['description']);
													echo $temp[sizeof($temp)-1];	
											    	?></td>
												<td><?php $success = 0; //success
													foreach ($operator_status_lastmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'XL'){
															if($value['PRODUCT_CODE']==$xl_code[$i]['talktime']){
																$success = $value['SUCCESS'];
																break;
															}
														}
													}
													echo number_format($success);
													?></td>
												<td><?php $failed = 0; //failed
													foreach ($operator_status_lastmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'XL'){
															if($value['PRODUCT_CODE']==$xl_code[$i]['talktime']){
																$failed = $value['FAILED'];
																break;
															}
														}
													}
													echo number_format($failed);
														?></td>
												<td><?php $percent = 0; //percent
													foreach ($operator_status_lastmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'XL'){
															if($value['PRODUCT_CODE']==$xl_code[$i]['talktime']){
																$percent = ($value['SUCCESS']/($value['SUCCESS']+$value['FAILED']))*100;
																break;
															}
														}
													}
													if($percent>=0){
														echo number_format($percent);
													}else{
														echo 0;
													}
													?></td>
											</tr>
										 <?php } ?>	
										</table>
									</div>
								</div>
							</div>
						</div>
						<!-- XL CURRENT MONTH -->
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="padding: 0% 0% 0 1%">
							<!-- TRANSAKSI TOTAL -->
							<div class="op-margin" style="background-color: white; padding: 2% 0 3% 0%; margin-left: -4%;">
								<h5 class="count_top" style="color: gray"><b>MtD</b></h5>
								<div class="count">
									<h1 style="font-weight: bold; color: black; margin-bottom:0px">
										<?php
											$sum=0;
											foreach ($operator_status_curmtd as $key => $value) {
												if($value['OPERATOR_NAME'] == 'XL'){
													$sum += $value['SUCCESS']+$value['FAILED'];
												}
											}
											echo number_format($sum);
										?>
									</h1>
								</div>
							</div>
							<!-- SUCCES FAILED PERCENT		 -->
							<div style="background-color: #eaf1fc; padding: 5% 0 5% 0">
								<div class="container-fluid">
									<div class="col-12 row" style="text-align: center">
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
											<h6 style="font-weight: bold">SUCCESS</h6>
											<h6>
												<?php 
													$sum=0;
													foreach ($operator_status_curmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'XL'){
															$sum += $value['SUCCESS'];
														}
													}
													echo number_format($sum);
												?>
											</h6>
										</div>
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
											<h6 style="font-weight: bold">FAILED</h6>
											<h6>
												<?php 
													$sum=0;
													foreach ($operator_status_curmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'XL'){
															$sum += $value['FAILED'];
														}
													}
													echo number_format($sum); 
												?>
											</h6>
										</div>
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
											<h6 style="font-weight: bold">%</h6>
											<h6>
												<?php
													$percent = 0; $sum_s=0; $sum_f=0;
													foreach ($operator_status_curmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'XL'){
															$sum_s+=$value['SUCCESS'];
															$sum_f+=$value['FAILED']; 
														}
													}
													if(($sum_s+$sum_f) == 0){
														echo 0;
													}else{
														$percent = ($sum_s/($sum_s+$sum_f))*100;
														echo number_format($percent);	
													}
												?>
											</h6>
										</div>
									</div>
								</div>
							</div>
							<!-- LIST OF NOMINAL -->
							<div style="background-color: #54b1f9; padding: 2% 0 5% 0; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px">
								<div class="container-fluid">
									<div class="col-12 row" style="text-align: center">
										<table class="table" style="color:white;text-align:center;" >
											<tr>
												<th>Nominal</th>
												<th>Success</th>
												<th>Failed</th>
												<th>%</th>
											</tr>

										<?php for ($i=0; $i <sizeof($xl_code) ; $i++) { ?> 
											<tr> 
												
												<td><?php //nominal
													$temp=explode(" ",$xl_code[$i]['description']);
													echo $temp[sizeof($temp)-1];	
											    	?></td>
												<td><?php $success = 0; //success
													foreach ($operator_status_curmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'XL'){
															if($value['PRODUCT_CODE']==$xl_code[$i]['talktime']){
																$success = $value['SUCCESS'];
																break;
															}
														}
													}
													echo number_format($success);
													?></td>
												<td><?php $failed = 0; //failed
													foreach ($operator_status_curmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'XL'){
															if($value['PRODUCT_CODE']==$xl_code[$i]['talktime']){
																$failed = $value['FAILED'];
																break;
															}
														}
													}
													echo number_format($failed);
														?></td>
												<td><?php $percent = 0; //percent
													foreach ($operator_status_curmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'XL'){
															if($value['PRODUCT_CODE']==$xl_code[$i]['talktime']){
																$percent = ($value['SUCCESS']/($value['SUCCESS']+$value['FAILED']))*100;
																break;
															}
														}
													}
													if($percent>=0){
														echo number_format($percent);
													}else{
														echo 0;
													}
													?></td>
											</tr>
										 <?php } ?>	
										</table>
									</div>
								</div>
							</div>
						</div>
						<!-- XL TODAY -->
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 op-today" style="padding: 0% 0% 0 1%">	
							<!-- TRANSAKSI TOTAL -->
							<div class="op-margin" style="background-color: white; padding: 2% 0 3% 0; margin-left: -4%;">
								<h5 class="count_top" style="color: gray"><b>Today</b></h5>
								<div class="count">
									<h1 style="font-weight: bold; color: black; margin-bottom:0px">
										<?php
											$sum=0;
											foreach ($operator_status as $key => $value) {
												if($value['OPERATOR_NAME'] == 'XL'){
													$sum += $value['SUCCESS']+$value['FAILED'];
												}
											}
											echo number_format($sum);
										?>
									</h1>
								</div>
							</div>
							<!-- SUCCES FAILED PERCENT		 -->
							<div style="background-color: #eaf1fc; padding: 5% 0 5% 0">
								<div class="container-fluid">
									<div class="col-12 row" style="text-align: center">
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
											<h6 style="font-weight: bold">SUCCESS</h6>
											<h6>
												<?php 
													$sum=0;
													foreach ($operator_status as $key => $value) {
														if($value['OPERATOR_NAME'] == 'XL'){
															$sum += $value['SUCCESS'];
														}
													}
													echo number_format($sum);
												?>
											</h6>
										</div>
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
											<h6 style="font-weight: bold">FAILED</h6>
											<h6>
												<?php 
													$sum=0;
													foreach ($operator_status as $key => $value) {
														if($value['OPERATOR_NAME'] == 'XL'){
															$sum += $value['FAILED'];
														}
													}
													echo number_format($sum); 
												?>
											</h6>
										</div>
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
											<h6 style="font-weight: bold">%</h6>
											<h6>
												<?php
													$percent = 0; $sum_s=0; $sum_f=0;
													foreach ($operator_status as $key => $value) {
														if($value['OPERATOR_NAME'] == 'XL'){
															$sum_s+=$value['SUCCESS'];
															$sum_f+=$value['FAILED']; 
														}
													}
													if(($sum_s+$sum_f) == 0){
														echo 0;
													}else{
														$percent = ($sum_s/($sum_s+$sum_f))*100;
														echo number_format($percent);	
													}
												?>
											</h6>
										</div>
									</div>
								</div>
							</div>
							<!-- LIST OF NOMINAL -->
							<div style="background-color: #54b1f9; padding: 2% 0 5% 0; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px">
								<div class="container-fluid">
									<div class="col-12 row" style="text-align: center">
										<table class="table" style="color:white;text-align:center;" >
											<tr>
												<th>Nominal</th>
												<th>Success</th>
												<th>Failed</th>
												<th>%</th>
											</tr>

										<?php for ($i=0; $i <sizeof($xl_code) ; $i++) { ?> 
											<tr> 
												
												<td><?php //nominal
													$temp=explode(" ",$xl_code[$i]['description']);
													echo $temp[sizeof($temp)-1];	
											    	?></td>
												<td><?php $success = 0; //success
													foreach ($operator_status as $key => $value) {
														if($value['OPERATOR_NAME'] == 'XL'){
															if($value['PRODUCT_CODE']==$xl_code[$i]['talktime']){
																$success = $value['SUCCESS'];
																break;
															}
														}
													}
													echo number_format($success);
													?></td>
												<td><?php $failed = 0; //failed
													foreach ($operator_status as $key => $value) {
														if($value['OPERATOR_NAME'] == 'XL'){
															if($value['PRODUCT_CODE']==$xl_code[$i]['talktime']){
																$failed = $value['FAILED'];
																break;
															}
														}
													}
													echo number_format($failed);
														?></td>
												<td><?php $percent = 0; //percent
													foreach ($operator_status as $key => $value) {
														if($value['OPERATOR_NAME'] == 'XL'){
															if($value['PRODUCT_CODE']==$xl_code[$i]['talktime']){
																$percent = ($value['SUCCESS']/($value['SUCCESS']+$value['FAILED']))*100;
																break;
															}
														}
													}
													if($percent>=0){
														echo number_format($percent);
													}else{
														echo 0;
													}
													?></td>
											</tr>
										 <?php } ?>	
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- SMARTFREN -->
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 5%; ">
						<!-- LOGO SMARTFREN -->
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: white; width:100%; height:20%; padding: 0% 0% 0% 1%;border-top-left-radius: 10px; border-top-right-radius: 10px">
							<img class="" src="<?php echo base_url()."/assets/";?>build/img/smartfren.jpg" alt="telkomsel_icon" height="100%">
						</div>
						<!-- SMARTFREN LAST MONTH -->
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="padding: 0% 0% 0 0%">	
							<!-- TRANSAKSI TOTAL -->
							<div style="background-color: white; padding: 2% 0 3% 0; border-top-left-radius: 0px; border-top-right-radius: 0px">
								<h5 class="count_top" style="color: gray"><b>LtD</b></h5>
								<div class="count">
									<h1 style="font-weight: bold; color: black; margin-bottom:0px">
										<?php
											$sum=0;
											foreach ($operator_status_lastmtd as $key => $value) {
												if($value['OPERATOR_NAME'] == 'SMARTFREN'){
													$sum += $value['SUCCESS']+$value['FAILED'];
												}
											}
											echo number_format($sum);
										?>
									</h1>
								</div>
							</div>
							<!-- SUCCES FAILED PERCENT		 -->
							<div style="background-color: #eaf1fc; padding: 5% 0 5% 0">
								<div class="container-fluid">
									<div class="col-12 row" style="text-align: center">
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
											<h6 style="font-weight: bold">SUCCESS</h6>
											<h6>
												<?php 
													$sum=0;
													foreach ($operator_status_lastmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'SMARTFREN'){
															$sum += $value['SUCCESS'];
														}
													}
													echo number_format($sum);
												?>
											</h6>
										</div>
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
											<h6 style="font-weight: bold">FAILED</h6>
											<h6>
												<?php 
													$sum=0;
													foreach ($operator_status_lastmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'SMARTFREN'){
															$sum += $value['FAILED'];
														}
													}
													echo number_format($sum); 
												?>
											</h6>
										</div>
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
											<h6 style="font-weight: bold">%</h6>
											<h6>
												<?php
													$percent = 0; $sum_s=0; $sum_f=0;
													foreach ($operator_status_lastmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'SMARTFREN'){
															$sum_s+=$value['SUCCESS'];
															$sum_f+=$value['FAILED']; 
														}
													}
													if(($sum_s+$sum_f) == 0){
														echo 0;
													}else{
														$percent = ($sum_s/($sum_s+$sum_f))*100;
														echo number_format($percent);	
													}
												?>
											</h6>
										</div>
									</div>
								</div>
							</div>
							<!-- LIST OF NOMINAL -->
							<div style="background-color: #54b1f9; padding: 2% 0 5% 0; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px">
								<div class="container-fluid">
									<div class="col-12 row" style="text-align: center">
										<table class="table" style="color:white;text-align:center;" >
											<tr>
												<th>Nominal</th>
												<th>Success</th>
												<th>Failed</th>
												<th>%</th>
											</tr>

										<?php for ($i=0; $i <sizeof($smartfren_code) ; $i++) { ?> 
											<tr> 
												
												<td><?php //nominal
													$temp=explode(" ",$smartfren_code[$i]['description']);
													echo $temp[sizeof($temp)-1];	
											    	?></td>
												<td><?php $success = 0; //success
													foreach ($operator_status_lastmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'SMARTFREN'){
															if($value['PRODUCT_CODE']==$smartfren_code[$i]['talktime']){
																$success = $value['SUCCESS'];
																break;
															}
														}
													}
													echo number_format($success);
													?></td>
												<td><?php $failed = 0; //failed
													foreach ($operator_status_lastmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'SMARTFREN'){
															if($value['PRODUCT_CODE']==$smartfren_code[$i]['talktime']){
																$failed = $value['FAILED'];
																break;
															}
														}
													}
													echo number_format($failed);
														?></td>
												<td><?php $percent = 0; //percent
													foreach ($operator_status_lastmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'SMARTFREN'){
															if($value['PRODUCT_CODE']==$smartfren_code[$i]['talktime']){
																$percent = ($value['SUCCESS']/($value['SUCCESS']+$value['FAILED']))*100;
																break;
															}
														}
													}
													if($percent>=0){
														echo number_format($percent);
													}else{
														echo 0;
													}
													?></td>
											</tr>
										 <?php } ?>	
										</table>
									</div>
								</div>
							</div>
						</div>
						<!-- SMARTFREN CURRENT MONTH -->
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="padding: 0% 0% 0 1%">
							<!-- TRANSAKSI TOTAL -->
							<div class="op-margin" style="background-color: white; padding: 2% 0 3% 0%; margin-left: -4%;">
								<h5 class="count_top" style="color: gray"><b>MtD</b></h5>
								<div class="count">
									<h1 style="font-weight: bold; color: black; margin-bottom:0px">
										<?php
											$sum=0;
											foreach ($operator_status_curmtd as $key => $value) {
												if($value['OPERATOR_NAME'] == 'SMARTFREN'){
													$sum += $value['SUCCESS']+$value['FAILED'];
												}
											}
											echo number_format($sum);
										?>
									</h1>
								</div>
							</div>
							<!-- SUCCES FAILED PERCENT		 -->
							<div style="background-color: #eaf1fc; padding: 5% 0 5% 0">
								<div class="container-fluid">
									<div class="col-12 row" style="text-align: center">
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
											<h6 style="font-weight: bold">SUCCESS</h6>
											<h6>
												<?php 
													$sum=0;
													foreach ($operator_status_curmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'SMARTFREN'){
															$sum += $value['SUCCESS'];
														}
													}
													echo number_format($sum);
												?>
											</h6>
										</div>
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
											<h6 style="font-weight: bold">FAILED</h6>
											<h6>
												<?php 
													$sum=0;
													foreach ($operator_status_curmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'SMARTFREN'){
															$sum += $value['FAILED'];
														}
													}
													echo number_format($sum); 
												?>
											</h6>
										</div>
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
											<h6 style="font-weight: bold">%</h6>
											<h6>
												<?php
													$percent = 0; $sum_s=0; $sum_f=0;
													foreach ($operator_status_curmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'SMARTFREN'){
															$sum_s+=$value['SUCCESS'];
															$sum_f+=$value['FAILED']; 
														}
													}
													if(($sum_s+$sum_f) == 0){
														echo 0;
													}else{
														$percent = ($sum_s/($sum_s+$sum_f))*100;
														echo number_format($percent);	
													}
												?>
											</h6>
										</div>
									</div>
								</div>
							</div>
							<!-- LIST OF NOMINAL -->
							<div style="background-color: #54b1f9; padding: 2% 0 5% 0; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px">
								<div class="container-fluid">
									<div class="col-12 row" style="text-align: center">
										<table class="table" style="color:white;text-align:center;" >
											<tr>
												<th>Nominal</th>
												<th>Success</th>
												<th>Failed</th>
												<th>%</th>
											</tr>

										<?php for ($i=0; $i <sizeof($smartfren_code) ; $i++) { ?> 
											<tr> 
												
												<td><?php //nominal
													$temp=explode(" ",$smartfren_code[$i]['description']);
													echo $temp[sizeof($temp)-1];	
											    	?></td>
												<td><?php $success = 0; //success
													foreach ($operator_status_curmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'SMARTFREN'){
															if($value['PRODUCT_CODE']==$smartfren_code[$i]['talktime']){
																$success = $value['SUCCESS'];
																break;
															}
														}
													}
													echo number_format($success);
													?></td>
												<td><?php $failed = 0; //failed
													foreach ($operator_status_curmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'SMARTFREN'){
															if($value['PRODUCT_CODE']==$smartfren_code[$i]['talktime']){
																$failed = $value['FAILED'];
																break;
															}
														}
													}
													echo number_format($failed);
														?></td>
												<td><?php $percent = 0; //percent
													foreach ($operator_status_curmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'SMARTFREN'){
															if($value['PRODUCT_CODE']==$smartfren_code[$i]['talktime']){
																$percent = ($value['SUCCESS']/($value['SUCCESS']+$value['FAILED']))*100;
																break;
															}
														}
													}
													if($percent>=0){
														echo number_format($percent);
													}else{
														echo 0;
													}
													?></td>
											</tr>
										 <?php } ?>	
										</table>
									</div>
								</div>
							</div>
						</div>
						<!-- SMARTFREN TODAY -->
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 op-today" style="padding: 0% 0% 0 1%">	
							<!-- TRANSAKSI TOTAL -->
							<div class="op-margin" style="background-color: white; padding: 2% 0 3% 0; margin-left: -4%;">
								<h5 class="count_top" style="color: gray"><b>Today</b></h5>
								<div class="count">
									<h1 style="font-weight: bold; color: black; margin-bottom:0px">
										<?php
											$sum=0;
											foreach ($operator_status as $key => $value) {
												if($value['OPERATOR_NAME'] == 'SMARTFREN'){
													$sum += $value['SUCCESS']+$value['FAILED'];
												}
											}
											echo number_format($sum);
										?>
									</h1>
								</div>
							</div>
							<!-- SUCCES FAILED PERCENT		 -->
							<div style="background-color: #eaf1fc; padding: 5% 0 5% 0">
								<div class="container-fluid">
									<div class="col-12 row" style="text-align: center">
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
											<h6 style="font-weight: bold">SUCCESS</h6>
											<h6>
												<?php 
													$sum=0;
													foreach ($operator_status as $key => $value) {
														if($value['OPERATOR_NAME'] == 'SMARTFREN'){
															$sum += $value['SUCCESS'];
														}
													}
													echo number_format($sum);
												?>
											</h6>
										</div>
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
											<h6 style="font-weight: bold">FAILED</h6>
											<h6>
												<?php 
													$sum=0;
													foreach ($operator_status as $key => $value) {
														if($value['OPERATOR_NAME'] == 'SMARTFREN'){
															$sum += $value['FAILED'];
														}
													}
													echo number_format($sum); 
												?>
											</h6>
										</div>
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
											<h6 style="font-weight: bold">%</h6>
											<h6>
												<?php
													$percent = 0; $sum_s=0; $sum_f=0;
													foreach ($operator_status as $key => $value) {
														if($value['OPERATOR_NAME'] == 'SMARTFREN'){
															$sum_s+=$value['SUCCESS'];
															$sum_f+=$value['FAILED']; 
														}
													}
													if(($sum_s+$sum_f) == 0){
														echo 0;
													}else{
														$percent = ($sum_s/($sum_s+$sum_f))*100;
														echo number_format($percent);	
													}
												?>
											</h6>
										</div>
									</div>
								</div>
							</div>
							<!-- LIST OF NOMINAL -->
							<div style="background-color: #54b1f9; padding: 2% 0 5% 0; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px">
								<div class="container-fluid">
									<div class="col-12 row" style="text-align: center">
										<table class="table" style="color:white;text-align:center;" >
											<tr>
												<th>Nominal</th>
												<th>Success</th>
												<th>Failed</th>
												<th>%</th>
											</tr>

										<?php for ($i=0; $i <sizeof($smartfren_code) ; $i++) { ?> 
											<tr> 
												
												<td><?php //nominal
													$temp=explode(" ",$smartfren_code[$i]['description']);
													echo $temp[sizeof($temp)-1];	
											    	?></td>
												<td><?php $success = 0; //success
													foreach ($operator_status as $key => $value) {
														if($value['OPERATOR_NAME'] == 'SMARTFREN'){
															if($value['PRODUCT_CODE']==$smartfren_code[$i]['talktime']){
																$success = $value['SUCCESS'];
																break;
															}
														}
													}
													echo number_format($success);
													?></td>
												<td><?php $failed = 0; //failed
													foreach ($operator_status as $key => $value) {
														if($value['OPERATOR_NAME'] == 'SMARTFREN'){
															if($value['PRODUCT_CODE']==$smartfren_code[$i]['talktime']){
																$failed = $value['FAILED'];
																break;
															}
														}
													}
													echo number_format($failed);
														?></td>
												<td><?php $percent = 0; //percent
													foreach ($operator_status as $key => $value) {
														if($value['OPERATOR_NAME'] == 'SMARTFREN'){
															if($value['PRODUCT_CODE']==$smartfren_code[$i]['talktime']){
																$percent = ($value['SUCCESS']/($value['SUCCESS']+$value['FAILED']))*100;
																break;
															}
														}
													}
													if($percent>=0){
														echo number_format($percent);
													}else{
														echo 0;
													}
													?></td>
											</tr>
										 <?php } ?>	
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- BOLT -->
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 5%; ">
						<!-- LOGO BOLT -->
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: white; width:100%; height:26%; padding: 0% 0% 0% 1%;border-top-left-radius: 10px; border-top-right-radius: 10px">

							<img class="" src="<?php echo base_url()."/assets/";?>build/img/bolt.jpg" alt="telkomsel_icon" height="100%" style="margin: auto;">
						</div>
						<!-- BOLT LAST MONTH -->
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="padding: 0% 0% 0 0%">	
							<!-- TRANSAKSI TOTAL -->
							<div style="background-color: white; padding: 2% 0 3% 0; border-top-left-radius: 0px; border-top-right-radius: 0px">
								<h5 class="count_top" style="color: gray"><b>LtD</b></h5>
								<div class="count">
									<h1 style="font-weight: bold; color: black; margin-bottom:0px">
										<?php
											$sum=0;
											foreach ($operator_status_lastmtd as $key => $value) {
												if($value['OPERATOR_NAME'] == 'BOLT'){
													$sum += $value['SUCCESS']+$value['FAILED'];
												}
											}
											echo number_format($sum);
										?>
									</h1>
								</div>
							</div>
							<!-- SUCCES FAILED PERCENT		 -->
							<div style="background-color: #eaf1fc; padding: 5% 0 5% 0">
								<div class="container-fluid">
									<div class="col-12 row" style="text-align: center">
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
											<h6 style="font-weight: bold">SUCCESS</h6>
											<h6>
												<?php 
													$sum=0;
													foreach ($operator_status_lastmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'BOLT'){
															$sum += $value['SUCCESS'];
														}
													}
													echo number_format($sum);
												?>
											</h6>
										</div>
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
											<h6 style="font-weight: bold">FAILED</h6>
											<h6>
												<?php 
													$sum=0;
													foreach ($operator_status_lastmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'BOLT'){
															$sum += $value['FAILED'];
														}
													}
													echo number_format($sum); 
												?>
											</h6>
										</div>
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
											<h6 style="font-weight: bold">%</h6>
											<h6>
												<?php
													$percent = 0; $sum_s=0; $sum_f=0;
													foreach ($operator_status_lastmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'BOLT'){
															$sum_s+=$value['SUCCESS'];
															$sum_f+=$value['FAILED']; 
														}
													}
													if(($sum_s+$sum_f) == 0){
														echo 0;
													}else{
														$percent = ($sum_s/($sum_s+$sum_f))*100;
														echo number_format($percent);	
													}
												?>
											</h6>
										</div>
									</div>
								</div>
							</div>
							<!-- LIST OF NOMINAL -->
							<div style="background-color: #54b1f9; padding: 2% 0 5% 0; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px">
								<div class="container-fluid">
									<div class="col-12 row" style="text-align: center">
										<table class="table" style="color:white;text-align:center;" >
											<tr>
												<th>Nominal</th>
												<th>Success</th>
												<th>Failed</th>
												<th>%</th>
											</tr>

										<?php for ($i=0; $i <sizeof($bolt_code) ; $i++) { ?> 
											<tr> 
												
												<td><?php //nominal
													$temp=explode(" ",$bolt_code[$i]['description']);
													echo $temp[sizeof($temp)-1];	
											    	?></td>
												<td><?php $success = 0; //success
													foreach ($operator_status_lastmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'BOLT'){
															if($value['PRODUCT_CODE']==$bolt_code[$i]['talktime']){
																$success = $value['SUCCESS'];
																break;
															}
														}
													}
													echo number_format($success);
													?></td>
												<td><?php $failed = 0; //failed
													foreach ($operator_status_lastmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'BOLT'){
															if($value['PRODUCT_CODE']==$bolt_code[$i]['talktime']){
																$failed = $value['FAILED'];
																break;
															}
														}
													}
													echo number_format($failed);
														?></td>
												<td><?php $percent = 0; //percent
													foreach ($operator_status_lastmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'BOLT'){
															if($value['PRODUCT_CODE']==$bolt_code[$i]['talktime']){
																$percent = ($value['SUCCESS']/($value['SUCCESS']+$value['FAILED']))*100;
																break;
															}
														}
													}
													if($percent>=0){
														echo number_format($percent);
													}else{
														echo 0;
													}
													?></td>
											</tr>
										 <?php } ?>	
										</table>
									</div>
								</div>
							</div>
						</div>
						<!-- BOLT CURRENT MONTH -->
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="padding: 0% 0% 0 1%">
							<!-- TRANSAKSI TOTAL -->
							<div class="op-margin" style="background-color: white; padding: 2% 0 3% 0%; margin-left: -4%;">
								<h5 class="count_top" style="color: gray"><b>MtD</b></h5>
								<div class="count">
									<h1 style="font-weight: bold; color: black; margin-bottom:0px">
										<?php
											$sum=0;
											foreach ($operator_status_curmtd as $key => $value) {
												if($value['OPERATOR_NAME'] == 'BOLT'){
													$sum += $value['SUCCESS']+$value['FAILED'];
												}
											}
											echo number_format($sum);
										?>
									</h1>
								</div>
							</div>
							<!-- SUCCES FAILED PERCENT		 -->
							<div style="background-color: #eaf1fc; padding: 5% 0 5% 0">
								<div class="container-fluid">
									<div class="col-12 row" style="text-align: center">
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
											<h6 style="font-weight: bold">SUCCESS</h6>
											<h6>
												<?php 
													$sum=0;
													foreach ($operator_status_curmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'BOLT'){
															$sum += $value['SUCCESS'];
														}
													}
													echo number_format($sum);
												?>
											</h6>
										</div>
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
											<h6 style="font-weight: bold">FAILED</h6>
											<h6>
												<?php 
													$sum=0;
													foreach ($operator_status_curmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'BOLT'){
															$sum += $value['FAILED'];
														}
													}
													echo number_format($sum); 
												?>
											</h6>
										</div>
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
											<h6 style="font-weight: bold">%</h6>
											<h6>
												<?php
													$percent = 0; $sum_s=0; $sum_f=0;
													foreach ($operator_status_curmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'BOLT'){
															$sum_s+=$value['SUCCESS'];
															$sum_f+=$value['FAILED']; 
														}
													}
													if(($sum_s+$sum_f) == 0){
														echo 0;
													}else{
														$percent = ($sum_s/($sum_s+$sum_f))*100;
														echo number_format($percent);	
													}
												?>
											</h6>
										</div>
									</div>
								</div>
							</div>
							<!-- LIST OF NOMINAL -->
							<div style="background-color: #54b1f9; padding: 2% 0 5% 0; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px">
								<div class="container-fluid">
									<div class="col-12 row" style="text-align: center">
										<table class="table" style="color:white;text-align:center;" >
											<tr>
												<th>Nominal</th>
												<th>Success</th>
												<th>Failed</th>
												<th>%</th>
											</tr>

										<?php for ($i=0; $i <sizeof($bolt_code) ; $i++) { ?> 
											<tr> 
												
												<td><?php //nominal
													$temp=explode(" ",$bolt_code[$i]['description']);
													echo $temp[sizeof($temp)-1];	
											    	?></td>
												<td><?php $success = 0; //success
													foreach ($operator_status_curmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'BOLT'){
															if($value['PRODUCT_CODE']==$bolt_code[$i]['talktime']){
																$success = $value['SUCCESS'];
																break;
															}
														}
													}
													echo number_format($success);
													?></td>
												<td><?php $failed = 0; //failed
													foreach ($operator_status_curmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'BOLT'){
															if($value['PRODUCT_CODE']==$bolt_code[$i]['talktime']){
																$failed = $value['FAILED'];
																break;
															}
														}
													}
													echo number_format($failed);
														?></td>
												<td><?php $percent = 0; //percent
													foreach ($operator_status_curmtd as $key => $value) {
														if($value['OPERATOR_NAME'] == 'BOLT'){
															if($value['PRODUCT_CODE']==$bolt_code[$i]['talktime']){
																$percent = ($value['SUCCESS']/($value['SUCCESS']+$value['FAILED']))*100;
																break;
															}
														}
													}
													if($percent>=0){
														echo number_format($percent);
													}else{
														echo 0;
													}
													?></td>
											</tr>
										 <?php } ?>	
										</table>
									</div>
								</div>
							</div>
						</div>
						<!-- BOLT TODAY -->
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 op-today" style="padding: 0% 0% 0 1%">	
							<!-- TRANSAKSI TOTAL -->
							<div class="op-margin" style="background-color: white; padding: 2% 0 3% 0; margin-left: -4%;">
								<h5 class="count_top" style="color: gray"><b>Today</b></h5>
								<div class="count">
									<h1 style="font-weight: bold; color: black; margin-bottom:0px">
										<?php
											$sum=0;
											foreach ($operator_status as $key => $value) {
												if($value['OPERATOR_NAME'] == 'BOLT'){
													$sum += $value['SUCCESS']+$value['FAILED'];
												}
											}
											echo number_format($sum);
										?>
									</h1>
								</div>
							</div>
							<!-- SUCCES FAILED PERCENT		 -->
							<div style="background-color: #eaf1fc; padding: 5% 0 5% 0">
								<div class="container-fluid">
									<div class="col-12 row" style="text-align: center">
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
											<h6 style="font-weight: bold">SUCCESS</h6>
											<h6>
												<?php 
													$sum=0;
													foreach ($operator_status as $key => $value) {
														if($value['OPERATOR_NAME'] == 'BOLT'){
															$sum += $value['SUCCESS'];
														}
													}
													echo number_format($sum);
												?>
											</h6>
										</div>
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
											<h6 style="font-weight: bold">FAILED</h6>
											<h6>
												<?php 
													$sum=0;
													foreach ($operator_status as $key => $value) {
														if($value['OPERATOR_NAME'] == 'BOLT'){
															$sum += $value['FAILED'];
														}
													}
													echo number_format($sum); 
												?>
											</h6>
										</div>
										<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
											<h6 style="font-weight: bold">%</h6>
											<h6>
												<?php
													$percent = 0; $sum_s=0; $sum_f=0;
													foreach ($operator_status as $key => $value) {
														if($value['OPERATOR_NAME'] == 'BOLT'){
															$sum_s+=$value['SUCCESS'];
															$sum_f+=$value['FAILED']; 
														}
													}
													if(($sum_s+$sum_f) == 0){
														echo 0;
													}else{
														$percent = ($sum_s/($sum_s+$sum_f))*100;
														echo number_format($percent);	
													}
												?>
											</h6>
										</div>
									</div>
								</div>
							</div>
							<!-- LIST OF NOMINAL -->
							<div style="background-color: #54b1f9; padding: 2% 0 5% 0; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px">
								<div class="container-fluid">
									<div class="col-12 row" style="text-align: center">
										<table class="table" style="color:white;text-align:center;" >
											<tr>
												<th>Nominal</th>
												<th>Success</th>
												<th>Failed</th>
												<th>%</th>
											</tr>

										<?php for ($i=0; $i <sizeof($bolt_code) ; $i++) { ?> 
											<tr> 
												
												<td><?php //nominal
													$temp=explode(" ",$bolt_code[$i]['description']);
													echo $temp[sizeof($temp)-1];	
											    	?></td>
												<td><?php $success = 0; //success
													foreach ($operator_status as $key => $value) {
														if($value['OPERATOR_NAME'] == 'BOLT'){
															if($value['PRODUCT_CODE']==$bolt_code[$i]['talktime']){
																$success = $value['SUCCESS'];
																break;
															}
														}
													}
													echo number_format($success);
													?></td>
												<td><?php $failed = 0; //failed
													foreach ($operator_status as $key => $value) {
														if($value['OPERATOR_NAME'] == 'BOLT'){
															if($value['PRODUCT_CODE']==$bolt_code[$i]['talktime']){
																$failed = $value['FAILED'];
																break;
															}
														}
													}
													echo number_format($failed);
														?></td>
												<td><?php $percent = 0; //percent
													foreach ($operator_status as $key => $value) {
														if($value['OPERATOR_NAME'] == 'BOLT'){
															if($value['PRODUCT_CODE']==$bolt_code[$i]['talktime']){
																$percent = ($value['SUCCESS']/($value['SUCCESS']+$value['FAILED']))*100;
																break;
															}
														}
													}
													if($percent>=0){
														echo number_format($percent);
													}else{
														echo 0;
													}
													?></td>
											</tr>
										 <?php } ?>	
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>

			</div>
			
			
