<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">

	<!-- refresh waktu (page) -->
	<!-- <meta http-equiv="refresh" content="60; url=<?php echo $_SERVER['PHP_SELF']; ?>">  -->

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Internal Dashboard Kioson</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url()."/assets/";?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Font Awesome -->
    <!-- <link href="<?php echo base_url()."/assets/";?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet"> -->

	<link href="<?php echo base_url()."/assets/";?>build/css/custom.css" rel="stylesheet">

  </head>

  <body class="nav-md" style="background-color: #f2f7f9">

	<!-- jQuery -->
	<script src="<?php echo base_url()."/assets/";?>vendors/jquery/dist/jquery.min.js"></script>

	<!-- ajax buat auto refresh (dibelakang sistem) -->
	<!-- 1. multiple ajax : bisa ngeload lebih dari satu function pada controller -->
	
	<script language="javascript"> 
		$(document).ready(function() {

			// $("#trans").load('<?php echo base_url();?>Welcome/transaksi');
			var refreshId = setInterval(function() {
				$("#trans").load('<?php echo base_url();?>Welcome/transaksi');
			}, 60000 * 5);

			// $("#op").load('<?php echo base_url();?>Welcome/operator');
			var refreshId = setInterval(function() {
				$("#op").load('<?php echo base_url();?>Welcome/operator');
				}, 50000 * 5);

			// $("#top").load('<?php echo base_url();?>Welcome/topup');
			var refreshId = setInterval(function() {
				$("#top").load('<?php echo base_url();?>Welcome/topup');
				}, 54000 * 5);

			// $("#plnpdam").load('<?php echo base_url();?>Welcome/plnpdam');
			var refreshId = setInterval(function() {
				$("#plnpdam").load('<?php echo base_url();?>Welcome/plnpdam');
				}, 56000 * 5);

			// $("#kcp").load('<?php echo base_url();?>Welcome/kcp');
			var refreshId = setInterval(function() {
				$("#kcp").load('<?php echo base_url();?>Welcome/kcp');
				}, 58000 * 5);

		});
	</script>


    <!-- Bootstrap -->
    <!-- <script src="<?php echo base_url()."/assets/";?>vendors/bootstrap/dist/js/bootstrap.min.js"></script> -->

    <!-- Custom Theme Scripts -->
    <!-- <script src="<?php echo base_url()."/assets/";?>build/js/custom.min.js"></script> -->
	
  </body>
</html>
