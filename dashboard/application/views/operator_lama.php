			<?php 

				function provider_sukses($operator , $amount){
					$lima = 0;
					foreach ($operator as $key => $value) {
						if ($value['OPERATOR_REFERENCE_ID'] != 'null' && $value['amount'] == $amount)
							{
								$lima++;
							}
					}

					return $lima;
				}

				function provider_gagal($operator , $amount){
					$lima = 0;
					foreach ($operator as $key => $value) {
						if ($value['OPERATOR_REFERENCE_ID'] = 'null' && $value['amount'] == $amount)
							{
								$lima++;
							}
					}

					return $lima;
				}

				function total_sukses_operator($operator){
					$sukses = 0;
					foreach ($operator as $key => $value) {
						if ($value['OPERATOR_REFERENCE_ID'] != 'null')
							{
								$sukses++;
							}
					}

					return $sukses;
				}

				function total_gagal_operator($operator){
					$gagal = 0;
					foreach ($operator as $key => $value) {
						if ($value['OPERATOR_REFERENCE_ID'] == 'null')
							{
								$gagal++;
							}
					}

					return $gagal;
				}

			?>

			<!-- Operator -->
			<div id="op">
				<!-- Baris 2 -->
				<div class="col-12 row" style="text-align: center; margin: 0">	<!-- class=".tile_waris1" -->
					<!-- 1 -->
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding: 3% 1% 0 1%">	<!-- class=".tile_waris1_count" -->
						<div style=" background-color: white; padding: 2% 0 2% 0; border-top-left-radius: 10px; border-top-right-radius: 10px">
							<img src="<?php echo base_url()."/assets/";?>build/img/indosat.jpg" width="20%" height="35px">
							<h5 class="count_top" style="color: black">Transaksi Indosat</h5>
							<div class="count">
								<h1 style="font-weight: bold; color: black">
									<?php
										echo number_format (count($operator_indosat));
									?>
								</h1>
							</div>
						</div>
								
						<div style="background-color: #eaf1fc; padding: 5% 0 5% 0">
							<div class="container-fluid">
								<div class="col-12 row" style="text-align: center">
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
										<h6 style="font-weight: bold">SUCCESS</h6>
										<h6>
											<?php 
												echo number_format (total_sukses_operator($operator_indosat));
											?>
										</h6>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
										<h6 style="font-weight: bold">FAILED</h6>
										<h6>
											<?php 
												echo number_format (total_gagal_operator($operator_indosat));
											?>
										</h6>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
										<h6 style="font-weight: bold">%</h6>
										<h6>
											<?php
												if( count($operator_indosat) != 0 )
												{
													$hasil = total_sukses_operator($operator_indosat) / count($operator_indosat) * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h6>
									</div>
								</div>
							</div>
						</div>
						
						<div style="background-color: #54b1f9; padding: 2% 0 5% 0; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px">
							<div class="container-fluid">
								<div class="col-12 row" style="text-align: center">
									<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="color: white"> 
										<h5 style="font-weight: bold">Nominal</h5>
										<h5 style="margin: 0">5rb</h5>
										<h5 style="margin: 0">10rb</h5>
										<h5 style="margin: 0">20rb</h5>
										<h5 style="margin: 0">25rb</h5>
										<h5 style="margin: 0">30rb</h5>
										<h5 style="margin: 0">50rb</h5>
										<h5 style="margin: 0">100rb</h5>
										<h5 style="margin: 0">150rb</h5>
										<h5 style="margin: 0">250rb</h5>
										<h5 style="margin: 0">500rb</h5>
										<h5 style="margin: 0">1jt</h5>
									</div>
									<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="color: white">
										<h5 style="font-weight: bold">Success</h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_indosat , '5000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_indosat , '10000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_indosat , '20000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_indosat , '25000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_indosat , '30000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_indosat , '50000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_indosat , '100000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_indosat , '150000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_indosat , '250000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_indosat , '500000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_indosat , '1000000')); ?></h5>
									</div>
									<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="color: white">
										<h5 style="font-weight: bold">Failed</h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_indosat , '5000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_indosat , '10000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_indosat , '20000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_indosat , '25000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_indosat , '30000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_indosat , '50000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_indosat , '100000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_indosat , '150000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_indosat , '250000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_indosat , '500000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_indosat , '1000000')); ?></h5>
									</div>
									<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="color: white">
										<h5 style="font-weight: bold">%</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_indosat , '5000') + provider_gagal($operator_indosat , '5000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_indosat , '5000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_indosat , '10000') + provider_gagal($operator_indosat , '10000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_indosat , '10000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_indosat , '20000') + provider_gagal($operator_indosat , '20000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_indosat , '20000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_indosat , '25000') + provider_gagal($operator_indosat , '25000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_indosat , '25000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_indosat , '30000') + provider_gagal($operator_indosat , '30000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_indosat , '30000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_indosat , '50000') + provider_gagal($operator_indosat , '50000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_indosat , '50000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_indosat , '100000') + provider_gagal($operator_indosat , '100000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_indosat , '100000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_indosat , '150000') + provider_gagal($operator_indosat , '150000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_indosat , '150000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_indosat , '250000') + provider_gagal($operator_indosat , '250000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_indosat , '250000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_indosat , '500000') + provider_gagal($operator_indosat , '500000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_indosat , '500000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_indosat , '1000000') + provider_gagal($operator_indosat , '1000000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_indosat , '1000000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- 2 -->
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding: 3% 1% 0 1%">	<!-- class=".tile_waris1_count" -->
						<div style="background-color: white; padding: 2% 0 2% 0; border-top-left-radius: 10px; border-top-right-radius: 10px">
							<img src="<?php echo base_url()."/assets/";?>build/img/smartfren.jpg" width="20%" height="35px">
							<h5 class="count_top" style="color: black">Transaksi Smartfren</h5>
							<div class="count">
								<h1 style="font-weight: bold; color: black">
									<?php
										echo number_format (count($operator_smartfren));
									?>
								</h1>
							</div>
						</div>
								
						<div style="background-color: #eaf1fc; padding: 5% 0 5% 0">
							<div class="container-fluid">
								<div class="col-12 row" style="text-align: center">
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
										<h6 style="font-weight: bold">SUCCESS</h6>
										<h6>
											<?php 
												echo number_format (total_sukses_operator($operator_smartfren));
											?>
										</h6>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
										<h6 style="font-weight: bold">FAILED</h6>
										<h6>
											<?php 
												echo number_format (total_gagal_operator($operator_smartfren));
											?>
										</h6>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
										<h6 style="font-weight: bold">%</h6>
										<h6>
											<?php
												if( count($operator_smartfren) != 0 )
												{
													$hasil = total_sukses_operator($operator_smartfren) / count($operator_smartfren) * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h6>
									</div>
								</div>
							</div>
						</div>
						
						<div style="background-color: #54b1f9; padding: 2% 0 5% 0; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px">
							<div class="container-fluid">
								<div class="col-12 row" style="text-align: center">
									<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="color: white"> 
										<h5 style="font-weight: bold">Nominal</h5>
										<h5 style="margin: 0">5rb</h5>
										<h5 style="margin: 0">10rb</h5>
										<h5 style="margin: 0">20rb</h5>
										<h5 style="margin: 0">25rb</h5>
										<h5 style="margin: 0">50rb</h5>
										<h5 style="margin: 0">60rb</h5>
										<h5 style="margin: 0">100rb</h5>
										<h5 style="margin: 0">150rb</h5>
										<h5 style="margin: 0">200rb</h5>
										<h5 style="margin: 0">300rb</h5>
										<h5 style="margin: 0">500rb</h5>
									</div>								
									<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="color: white">
										<h5 style="font-weight: bold">Success</h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_smartfren , '5000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_smartfren , '10000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_smartfren , '20000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_smartfren , '25000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_smartfren , '50000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_smartfren , '60000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_smartfren , '100000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_smartfren , '150000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_smartfren , '200000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_smartfren , '300000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_smartfren , '500000')); ?></h5>
									</div>
									<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="color: white">
										<h5 style="font-weight: bold">Failed</h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_smartfren , '5000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_smartfren , '10000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_smartfren , '20000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_smartfren , '25000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_smartfren , '50000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_smartfren , '60000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_smartfren , '100000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_smartfren , '150000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_smartfren , '200000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_smartfren , '300000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_smartfren , '500000')); ?></h5>
									</div>
									<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="color: white">
										<h5 style="font-weight: bold">%</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_smartfren , '5000') + provider_gagal($operator_smartfren , '5000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_smartfren , '5000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_smartfren , '10000') + provider_gagal($operator_smartfren , '10000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_smartfren , '10000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_smartfren , '20000') + provider_gagal($operator_smartfren , '20000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_smartfren , '20000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_smartfren , '25000') + provider_gagal($operator_smartfren , '25000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_smartfren , '25000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_smartfren , '50000') + provider_gagal($operator_smartfren , '50000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_smartfren , '50000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_smartfren , '60000') + provider_gagal($operator_smartfren , '60000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_smartfren , '60000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_smartfren , '100000') + provider_gagal($operator_smartfren , '100000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_smartfren , '100000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_smartfren , '150000') + provider_gagal($operator_smartfren , '150000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_smartfren , '150000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_smartfren , '200000') + provider_gagal($operator_smartfren , '200000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_smartfren , '200000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_smartfren , '300000') + provider_gagal($operator_smartfren , '300000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_smartfren , '300000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_smartfren , '500000') + provider_gagal($operator_smartfren , '500000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_smartfren , '500000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- 3 -->
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding: 3% 1% 0 1%">	<!-- class=".tile_waris1_count" -->
						<div style="background-color: white; padding: 2% 0 2% 0; border-top-left-radius: 10px; border-top-right-radius: 10px">
							<img src="<?php echo base_url()."/assets/";?>build/img/three.png" width="20%" height="35px">
							<h5 class="count_top" style="color: black">Transaksi Three</h5>
							<div class="count">
								<h1 style="font-weight: bold; color: black">
									<?php
										echo number_format (count($operator_three));
									?>
								</h1>
							</div>
						</div>
								
						<div style="background-color: #eaf1fc; padding: 5% 0 5% 0">
							<div class="container-fluid">
								<div class="col-12 row" style="text-align: center">
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
										<h6 style="font-weight: bold">SUCCESS</h6>
										<h6>
											<?php 
												echo number_format (total_sukses_operator($operator_three));
											?>
										</h6>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
										<h6 style="font-weight: bold">FAILED</h6>
										<h6>
											<?php 
												echo number_format (total_gagal_operator($operator_three));
											?>
										</h6>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
										<h6 style="font-weight: bold">%</h6>
										<h6>
											<?php
												if( count($operator_three) != 0 )
												{
													$hasil = total_sukses_operator($operator_three) / count($operator_three) * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h6>
									</div>
								</div>
							</div>
						</div>
						
						<div style="background-color: #54b1f9; padding: 2% 0 5% 0; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px">
							<div class="container-fluid">
								<div class="col-12 row" style="text-align: center">
									<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="color: white"> 
										<h5 style="font-weight: bold">Nominal</h5>
										<h5 style="margin: 0">1rb</h5>
										<h5 style="margin: 0">5rb</h5>
										<h5 style="margin: 0">10rb</h5>
										<h5 style="margin: 0">20rb</h5>
										<h5 style="margin: 0">25rb</h5>
										<h5 style="margin: 0">50rb</h5>
										<h5 style="margin: 0">100rb</h5>
										<h5 style="margin: 0">150rb</h5>
										<h5 style="margin: 0">300rb</h5>
										<h5 style="margin: 0">500rb</h5>
										<h5 style="margin: 0">1jt</h5>
									</div>
									<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="color: white">
										<h5 style="font-weight: bold">Success</h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_three , '1000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_three , '5000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_three , '10000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_three , '20000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_three , '25000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_three , '50000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_three , '100000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_three , '150000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_three , '300000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_three , '500000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_three , '1000000')); ?></h5>
									</div>
									<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="color: white">
										<h5 style="font-weight: bold">Failed</h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_three , '1000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_three , '5000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_three , '10000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_three , '20000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_three , '25000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_three , '50000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_three , '100000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_three , '150000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_three , '300000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_three , '500000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_three , '1000000')); ?></h5>
									</div>
									<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="color: white">
										<h5 style="font-weight: bold">%</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_three , '1000') + provider_gagal($operator_three , '1000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_three , '1000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_three , '5000') + provider_gagal($operator_three , '5000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_three , '5000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_three , '10000') + provider_gagal($operator_three , '10000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_three , '10000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_three , '20000') + provider_gagal($operator_three , '20000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_three , '20000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_three , '25000') + provider_gagal($operator_three , '25000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_three , '25000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_three , '50000') + provider_gagal($operator_three , '50000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_three , '50000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_three , '100000') + provider_gagal($operator_three , '100000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_three , '100000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_three , '150000') + provider_gagal($operator_three , '150000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_three , '150000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_three , '300000') + provider_gagal($operator_three , '300000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_three , '300000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_three , '500000') + provider_gagal($operator_three , '500000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_three , '500000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_three , '1000000') + provider_gagal($operator_three , '1000000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_three , '1000000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Baris 3 -->
				<div class="col-12 row" style="text-align: center; margin: 0">	<!-- class=".tile_waris2" -->
					<!-- 1 -->
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding: 3% 1% 0 1%">	<!-- class=".tile_waris2_count" -->
						<div style="background-color: white; padding: 2% 0 2% 0; border-top-left-radius: 10px; border-top-right-radius: 10px">
							<img src="<?php echo base_url()."/assets/";?>build/img/bolt.jpg" width="20%" height="35px">
							<h5 class="count_top" style="color: black">Transaksi Bolt</h5>
							<div class="count">
								<h1 style="font-weight: bold; color: black">
									<?php
										echo number_format (count($operator_bolt));
									?>
								</h1>
							</div>
						</div>
								
						<div style="background-color: #eaf1fc; padding: 5% 0 5% 0">
							<div class="container-fluid">
								<div class="col-12 row" style="text-align: center">
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
										<h6 style="font-weight: bold">SUCCESS</h6>
										<h6>
											<?php 
												echo number_format (total_sukses_operator($operator_bolt));
											?>
										</h6>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
										<h6 style="font-weight: bold">FAILED</h6>
											<?php 
												echo number_format (total_gagal_operator($operator_bolt));
											?>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
										<h6 style="font-weight: bold">%</h6>
										<h6>
											<?php
												if( count($operator_bolt) != 0 )
												{
													$hasil = total_sukses_operator($operator_bolt) / count($operator_bolt) * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h6>
									</div>
								</div>
							</div>
						</div>
						
						<div style="background-color: #54b1f9; padding: 2% 0 5% 0; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px; height: 216px">
							<div class="container-fluid">
								<div class="col-12 row" style="text-align: center">
									<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="color: white"> 
										<h5 style="font-weight: bold">Nominal</h5>
										<h5 style="margin: 0">25rb</h5>
										<h5 style="margin: 0">50rb</h5>
										<h5 style="margin: 0">100rb</h5>
										<h5 style="margin: 0">150rb</h5>
										<h5 style="margin: 0">200rb</h5>
									</div>
									<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="color: white">
										<h5 style="font-weight: bold">Success</h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_bolt , '25000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_bolt , '50000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_bolt , '100000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_bolt , '150000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_bolt , '200000')); ?></h5>
									</div>
									<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="color: white">
										<h5 style="font-weight: bold">Failed</h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_bolt , '25000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_bolt , '50000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_bolt , '100000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_bolt , '150000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_bolt , '200000')); ?></h5>
									</div>
									<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="color: white">
										<h5 style="font-weight: bold">%</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_bolt , '25000') + provider_gagal($operator_bolt , '25000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_bolt , '25000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_bolt , '50000') + provider_gagal($operator_bolt , '50000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_bolt , '50000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_bolt , '100000') + provider_gagal($operator_bolt , '100000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_bolt , '100000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_bolt , '150000') + provider_gagal($operator_bolt , '150000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_bolt , '150000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_bolt , '200000') + provider_gagal($operator_bolt , '200000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_bolt , '200000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- 2 -->
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding: 3% 1% 0 1%">	<!-- class=".tile_waris2_count" -->
						<div style="background-color: white; padding: 2% 0 2% 0; border-top-left-radius: 10px; border-top-right-radius: 10px">
							<img src="<?php echo base_url()."/assets/";?>build/img/XL.jpg" width="20%" height="35px">
							<h5 class="count_top" style="color: black">Transaksi XL</h5>
							<div class="count">
								<h1 style="font-weight: bold; color: black">
									<?php
										echo number_format (count($operator_xl));
									?>
								</h1>
							</div>
						</div>
								
						<div style="background-color: #eaf1fc; padding: 5% 0 5% 0">
							<div class="container-fluid">
								<div class="col-12 row" style="text-align: center">
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
										<h6 style="font-weight: bold">SUCCESS</h6>
										<h6>
											<?php 
												echo number_format (total_sukses_operator($operator_xl));
											?>
										</h6>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
										<h6 style="font-weight: bold">FAILED</h6>
										<h6>
											<?php 
												echo number_format (total_gagal_operator($operator_xl));
											?>
										</h6>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
										<h6 style="font-weight: bold">%</h6>
										<h6>
											<?php
												if( count($operator_xl) != 0 )
												{
													$hasil = total_sukses_operator($operator_xl) / count($operator_xl) * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h6>
									</div>
								</div>
							</div>
						</div>
						
						<div style="background-color: #54b1f9; padding: 2% 0 5% 0; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px; height: 216px">
							<div class="container-fluid">
								<div class="col-12 row" style="text-align: center">
									<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="color: white"> 
										<h5 style="font-weight: bold">Nominal</h5>
										<h5 style="margin: 0">5rb</h5>
										<h5 style="margin: 0">10rb</h5>
										<h5 style="margin: 0">25rb</h5>
										<h5 style="margin: 0">50rb</h5>
										<h5 style="margin: 0">100rb</h5>
										<h5 style="margin: 0">200rb</h5>
										<h5 style="margin: 0">300rb</h5>
										<h5 style="margin: 0">500rb</h5>
										<h5 style="margin: 0">1jt</h5>
									</div>				
									<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="color: white">
										<h5 style="font-weight: bold">Success</h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_xl , '5000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_xl , '10000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_xl , '25000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_xl , '50000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_xl , '100000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_xl , '200000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_xl , '300000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_xl , '500000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_xl , '1000000')); ?></h5>
									</div>
									<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="color: white">
										<h5 style="font-weight: bold">Failed</h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_xl , '5000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_xl , '10000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_xl , '25000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_xl , '50000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_xl , '100000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_xl , '200000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_xl , '300000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_xl , '500000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_xl , '1000000')); ?></h5>
									</div>
									<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="color: white">
										<h5 style="font-weight: bold">%</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_xl , '5000') + provider_gagal($operator_xl , '5000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_xl , '5000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_xl , '10000') + provider_gagal($operator_xl , '10000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_xl , '10000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_xl , '25000') + provider_gagal($operator_xl , '25000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_xl , '25000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_xl , '50000') + provider_gagal($operator_xl , '50000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_xl , '50000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_xl , '100000') + provider_gagal($operator_xl , '100000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_xl , '100000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_xl , '200000') + provider_gagal($operator_xl , '200000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_xl , '200000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_xl , '300000') + provider_gagal($operator_xl , '300000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_xl , '300000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_xl , '500000') + provider_gagal($operator_xl , '500000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_xl , '500000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_xl , '1000000') + provider_gagal($operator_xl , '1000000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_xl , '1000000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- 3 -->
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding: 3% 1% 0 1%">	<!-- class=".tile_waris2_count" -->
						<div style="background-color: white; padding: 2% 0 2% 0; border-top-left-radius: 10px; border-top-right-radius: 10px">
							<img src="<?php echo base_url()."/assets/";?>build/img/telkomsel.jpg" width="20%" height="35px">
							<h5 class="count_top" style="color: black">Transaksi Telkomsel</h5>
							<div class="count">
								<h1 style="font-weight: bold; color: black">
									<?php
										echo number_format (count($operator_telkomsel));
									?>
								</h1>
							</div>
						</div>
								
						<div style="background-color: #eaf1fc; padding: 5% 0 5% 0">
							<div class="container-fluid">
								<div class="col-12 row" style="text-align: center">
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
										<h6 style="font-weight: bold">SUCCESS</h6>
										<h6>
											<?php 
												echo number_format (total_sukses_operator($operator_telkomsel)); 
											?>
										</h6>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid white">
										<h6 style="font-weight: bold">FAILED</h6>
										<h6>
											<?php 
												echo number_format (total_gagal_operator($operator_telkomsel)); 
											?>
										</h6>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
										<h6 style="font-weight: bold">%</h6>
										<h6>
											<?php
												if( count($operator_telkomsel) != 0 )
												{
													$hasil = total_sukses_operator($operator_telkomsel) / count($operator_telkomsel) * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h6>
									</div>
								</div>
							</div>
						</div>
						
						<div style="background-color: #54b1f9; padding: 2% 0 5% 0; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px">
							<div class="container-fluid">
								<div class="col-12 row" style="text-align: center">
									<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="color: white"> 
										<h5 style="font-weight: bold">Nominal</h5>
										<h5 style="margin: 0">10rb</h5>
										<h5 style="margin: 0">20rb</h5>
										<h5 style="margin: 0">25rb</h5>
										<h5 style="margin: 0">40rb</h5>
										<h5 style="margin: 0">50rb</h5>
										<h5 style="margin: 0">100rb</h5>
										<h5 style="margin: 0">150rb</h5>
										<h5 style="margin: 0">200rb</h5>
										<h5 style="margin: 0">300rb</h5>
										<h5 style="margin: 0">500rb</h5>
									</div>
									<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="color: white">
										<h5 style="font-weight: bold">Success</h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_telkomsel , '10000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_telkomsel , '20000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_telkomsel , '25000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_telkomsel , '40000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_telkomsel , '50000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_telkomsel , '100000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_telkomsel , '150000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_telkomsel , '200000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_telkomsel , '300000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_sukses($operator_telkomsel , '500000')); ?></h5>
									</div>
									<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="color: white">
										<h5 style="font-weight: bold">Failed</h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_telkomsel , '10000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_telkomsel , '20000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_telkomsel , '25000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_telkomsel , '40000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_telkomsel , '50000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_telkomsel , '100000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_telkomsel , '150000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_telkomsel , '200000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_telkomsel , '300000')); ?></h5>
										<h5 style="margin: 0"><?php echo number_format (provider_gagal($operator_telkomsel , '500000')); ?></h5>
									</div>
									<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="color: white">
										<h5 style="font-weight: bold">%</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_telkomsel , '10000') + provider_gagal($operator_telkomsel , '10000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_telkomsel , '10000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_telkomsel , '20000') + provider_gagal($operator_telkomsel , '20000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_telkomsel , '20000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_telkomsel , '25000') + provider_gagal($operator_telkomsel , '25000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_telkomsel , '25000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_telkomsel , '40000') + provider_gagal($operator_telkomsel , '40000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_telkomsel , '40000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_telkomsel , '50000') + provider_gagal($operator_telkomsel , '50000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_telkomsel , '50000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												} 
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_telkomsel , '100000') + provider_gagal($operator_telkomsel , '100000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_telkomsel , '100000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												} 
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_telkomsel , '150000') + provider_gagal($operator_telkomsel , '150000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_telkomsel , '150000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												} 
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_telkomsel , '200000') + provider_gagal($operator_telkomsel , '200000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_telkomsel , '200000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}                 
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_telkomsel , '300000') + provider_gagal($operator_telkomsel , '300000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_telkomsel , '300000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}								
											?>
										</h5>
										<h5 style="margin: 0">
											<?php
												$total = provider_sukses($operator_telkomsel , '500000') + provider_gagal($operator_telkomsel , '500000');
												
												if( $total != 0 )
												{
													$hasil = provider_sukses($operator_telkomsel , '500000') / $total * 100;
													echo number_format($hasil);
												}
												else{
													echo 0;
												}
											?>
										</h5>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			
