			<!-- Pulsa -->
			<!-- Baris 1 -->
          	<div id="trans" class="col-12 row" style="text-align: center; margin: 0">	<!-- class=".tile_count" -->
				<h3 style="text-align: center; padding-top: 3%; margin: 0">
					<?php
						date_default_timezone_set('Asia/Jakarta');
						echo "Last Update " . date('d-m-Y H:i:s') . "<br>";
					?>
				</h3>
				
				<!-- 1 MR -->
				<!-- Last Month -->
            	<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="padding: 3% 1% 0 1%">	<!-- class=".tile_stats_count" -->
					<div style="background-color: #ff8700; padding: 2% 0 2% 0; border-top-left-radius: 10px; border-top-right-radius: 10px">
						<h5 class="count_top" style="color: white">Mobile Recharge Transaction<br>(LtD)</h5>
						<div class="count">
							<h1 style="font-weight: bold; color: white">
								<?php							
								// echo number_format($pulsa_today[0]["total"]);
									$total_pulsa = $last_month[0]["MR_sukses"] + $last_month[0]["MR_gagal"];
									echo number_format($total_pulsa);
								?>
							</h1>
						</div>
					</div>

					<div style="background-color: white; padding: 5% 0 5% 0; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px">
						<div class="container-fluid">
							<div class="col-12 row" style="text-align: center">
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid #f4f4f4">
									<h6 style="font-weight: bold">SUCCESS</h6>
									<h6>
									<?php
									echo number_format($last_month[0]["MR_sukses"]);
									?>
									</h6>
								</div>
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid #f4f4f4">
									<h6 style="font-weight: bold">FAILED</h6>
									<h6>
									<?php
									echo number_format($last_month[0]["MR_gagal"]);
									?>
								</h6>
								</div>
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
									<h6 style="font-weight: bold">%</h6>
									<h6>
										<?php
											$total_last_month = $last_month[0]["MR_sukses"] + $last_month[0]["MR_gagal"];


											if(($last_month[0]["MR_sukses"] && $last_month[0]["MR_gagal"] > 0)
											||($last_month[0]["MR_sukses"] || $last_month[0]["MR_gagal"] > 0))
											{
												$hasil = ($last_month[0]["MR_sukses"]/$total_last_month)*100;
											echo number_format($hasil);
											}
											else{
											echo 0;
											}
										?>
									</h6>
								</div>
							</div>
						</div>
					</div>
				</div>				

				<!-- This Month -->
            	<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="padding: 3% 1% 0 1%">	<!-- class=".tile_stats_count" -->
					<div style="background-color: #ff8700; padding: 2% 0 2% 0; border-top-left-radius: 10px; border-top-right-radius: 10px">
						<h5 class="count_top" style="color: white">Mobile Recharge Transaction<br>(MtD)</h5>
						<div class="count">
							<h1 style="font-weight: bold; color: white">
								<?php							
								// echo number_format($pulsa_today[0]["total"]);
									$total_pulsa = $this_month[0]["MR_sukses"] + $this_month[0]["MR_gagal"];
									echo number_format($total_pulsa);
								?>
							</h1>
						</div>
					</div>

					<div style="background-color: white; padding: 5% 0 5% 0; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px">
						<div class="container-fluid">
							<div class="col-12 row" style="text-align: center">
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid #f4f4f4">
									<h6 style="font-weight: bold">SUCCESS</h6>
									<h6>
									<?php
									echo number_format($this_month[0]["MR_sukses"]);
									?>
									</h6>
								</div>
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid #f4f4f4">
									<h6 style="font-weight: bold">FAILED</h6>
									<h6>
									<?php
									echo number_format($this_month[0]["MR_gagal"]);
									?>
								</h6>
								</div>
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
									<h6 style="font-weight: bold">%</h6>
									<h6>
										<?php
											$total_pulsa = $this_month[0]["MR_sukses"] + $this_month[0]["MR_gagal"];


											if(($this_month[0]["MR_sukses"] && $this_month[0]["MR_gagal"] > 0)
											||($this_month[0]["MR_sukses"] || $this_month[0]["MR_gagal"] > 0))
											{
												$hasil = ($this_month[0]["MR_sukses"]/$total_pulsa)*100;
											echo number_format($hasil);
											}
											else{
											echo 0;
											}
										?>
									</h6>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Today -->
            	<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="padding: 3% 1% 0 1%">	<!-- class=".tile_stats_count" -->
					<div style="background-color: #ff8700; padding: 2% 0 2% 0; border-top-left-radius: 10px; border-top-right-radius: 10px">
						<h5 class="count_top" style="color: white">Mobile Recharge Transaction<br>(Today)</h5>
						<div class="count">
							<h1 style="font-weight: bold; color: white">
								<?php							
								echo number_format($pulsa_today[0]["total"]);	
								?>
							</h1>
						</div>
					</div>

					<div style="background-color: white; padding: 5% 0 5% 0; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px">
						<div class="container-fluid">
							<div class="col-12 row" style="text-align: center">
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid #f4f4f4">
									<h6 style="font-weight: bold">SUCCESS</h6>
									<h6>
									<?php
									echo number_format($pulsa_today[0]["sukses"]);
									?>
									</h6>
								</div>
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid #f4f4f4">
									<h6 style="font-weight: bold">FAILED</h6>
									<h6>
									<?php
									echo number_format($pulsa_today[0]["gagal"]);
									?>
								</h6>
								</div>
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
									<h6 style="font-weight: bold">%</h6>
									<h6>
										<?php										
											if(($pulsa_today[0]["sukses"] && $pulsa_today[0]["gagal"] > 0)
											||($pulsa_today[0]["sukses"] || $pulsa_today[0]["gagal"] > 0))
											{
												$hasil = ($pulsa_today[0]["sukses"]/$pulsa_today[0]["total"])*100;
											echo number_format($hasil);
											}
											else{
											echo 0;
											}
										?>
									</h6>
								</div>
							</div>
						</div>
					</div>
				</div>


				<!-- 2 DC -->
				<!-- Last Month -->
            	<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="padding: 3% 1% 0 1%">	<!-- class=".tile_stats_count" -->
					<div style="background-color: #ff8700; padding: 2% 0 2% 0; border-top-left-radius: 10px; border-top-right-radius: 10px">
						<h5 class="count_top" style="color: white">Data Card Transaction<br>(LtD)</h5>
						<div class="count">
							<h1 style="font-weight: bold; color: white">
								<?php							
								// echo number_format($pulsa_today[0]["total"]);
									$total_pulsa = $last_month[0]["DC_sukses"] + $last_month[0]["DC_gagal"];
									echo number_format($total_pulsa);
								?>
							</h1>
						</div>
					</div>

					<div style="background-color: white; padding: 5% 0 5% 0; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px">
						<div class="container-fluid">
							<div class="col-12 row" style="text-align: center">
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid #f4f4f4">
									<h6 style="font-weight: bold">SUCCESS</h6>
									<h6>
									<?php
									echo number_format($last_month[0]["DC_sukses"]);
									?>
									</h6>
								</div>
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid #f4f4f4">
									<h6 style="font-weight: bold">FAILED</h6>
									<h6>
									<?php
									echo number_format($last_month[0]["DC_gagal"]);
									?>
								</h6>
								</div>
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
									<h6 style="font-weight: bold">%</h6>
									<h6>
										<?php
											$total_last_month = $last_month[0]["DC_sukses"] + $last_month[0]["DC_gagal"];


											if(($last_month[0]["DC_sukses"] && $last_month[0]["DC_gagal"] > 0)
											||($last_month[0]["DC_sukses"] || $last_month[0]["DC_gagal"] > 0))
											{
												$hasil = ($last_month[0]["DC_sukses"]/$total_last_month)*100;
											echo number_format($hasil);
											}
											else{
											echo 0;
											}
										?>
									</h6>
								</div>
							</div>
						</div>
					</div>
				</div>				

				<!-- This Month -->
            	<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="padding: 3% 1% 0 1%">	<!-- class=".tile_stats_count" -->
					<div style="background-color: #ff8700; padding: 2% 0 2% 0; border-top-left-radius: 10px; border-top-right-radius: 10px">
						<h5 class="count_top" style="color: white">Data Card Transaction<br>(MtD)</h5>
						<div class="count">
							<h1 style="font-weight: bold; color: white">
								<?php							
								// echo number_format($pulsa_today[0]["total"]);
									$total_pulsa = $this_month[0]["DC_sukses"] + $this_month[0]["DC_gagal"];
									echo number_format($total_pulsa);
								?>
							</h1>
						</div>
					</div>

					<div style="background-color: white; padding: 5% 0 5% 0; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px">
						<div class="container-fluid">
							<div class="col-12 row" style="text-align: center">
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid #f4f4f4">
									<h6 style="font-weight: bold">SUCCESS</h6>
									<h6>
									<?php
									echo number_format($this_month[0]["DC_sukses"]);
									?>
									</h6>
								</div>
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid #f4f4f4">
									<h6 style="font-weight: bold">FAILED</h6>
									<h6>
									<?php
									echo number_format($this_month[0]["DC_gagal"]);
									?>
								</h6>
								</div>
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
									<h6 style="font-weight: bold">%</h6>
									<h6>
										<?php
											$total_pulsa = $this_month[0]["DC_sukses"] + $this_month[0]["DC_gagal"];


											if(($this_month[0]["DC_sukses"] && $this_month[0]["DC_gagal"] > 0)
											||($this_month[0]["DC_sukses"] || $this_month[0]["DC_gagal"] > 0))
											{
												$hasil = ($this_month[0]["DC_sukses"]/$total_pulsa)*100;
											echo number_format($hasil);
											}
											else{
											echo 0;
											}
										?>
									</h6>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Today -->
            	<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="padding: 3% 1% 0 1%">	<!-- class=".tile_stats_count" -->
					<div style="background-color: #ff8700; padding: 2% 0 2% 0; border-top-left-radius: 10px; border-top-right-radius: 10px">
						<h5 class="count_top" style="color: white">Data Card Transaction<br>(Today)</h5>
						<div class="count">
							<h1 style="font-weight: bold; color: white">
								<?php							
								echo number_format($data_today[0]["total"]);	
								?>
							</h1>
						</div>
					</div>

					<div style="background-color: white; padding: 5% 0 5% 0; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px">
						<div class="container-fluid">
							<div class="col-12 row" style="text-align: center">
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid #f4f4f4">
									<h6 style="font-weight: bold">SUCCESS</h6>
									<h6>
									<?php
									echo number_format($data_today[0]["sukses"]);
									?>
									</h6>
								</div>
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid #f4f4f4">
									<h6 style="font-weight: bold">FAILED</h6>
									<h6>
									<?php
									echo number_format($data_today[0]["gagal"]);
									?>
								</h6>
								</div>
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
									<h6 style="font-weight: bold">%</h6>
									<h6>
										<?php										
											if(($data_today[0]["sukses"] && $data_today[0]["gagal"] > 0)
											||($data_today[0]["sukses"] || $data_today[0]["gagal"] > 0))
											{
												$hasil = ($data_today[0]["sukses"]/$data_today[0]["total"])*100;
											echo number_format($hasil);
											}
											else{
											echo 0;
											}
										?>
									</h6>
								</div>
							</div>
						</div>
					</div>
				</div>

				
						
				




          	</div>
