<div id="plnpdam" class="row " style="text-align: center; margin-left: 0; margin-right: 0; padding-top: 3%;">	
	<!-- PLN -->
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0%;padding-right: 0%;">
		<!-- Last Month -->
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="padding-left:1%;">
			<div style="background-color: #ff8700; padding: 2% 0 2% 0; border-top-left-radius: 10px; border-top-right-radius: 10px">
				<h5 class="count_top" style="color: white">Total PLN<br>LTD</h5>
				<div class="count">
					<h1 style="font-weight: bold; color: white">
						<?php 
							if (date("d") <= 10) {
								$prepaid = $lastmonth_pln['prepaid1'];
								$postpaid = $lastmonth_pln['postpaid1'];
								echo number_format($postpaid + $prepaid);
							} 
							else if (date("d") > 10 && date("d") <= 20) {
								$prepaid = $lastmonth_pln['prepaid1'] + $lastmonth_pln['prepaid2'];
								$postpaid = $lastmonth_pln['postpaid1'] + $lastmonth_pln['postpaid2'];
								echo number_format($postpaid + $prepaid);
							}
							else {
								$prepaid = $lastmonth_pln['prepaid1'] + $lastmonth_pln['prepaid2'] + $lastmonth_pln['prepaid3'];
								$postpaid = $lastmonth_pln['postpaid1'] + $lastmonth_pln['postpaid2'] + $lastmonth_pln['postpaid3'];
							echo number_format($postpaid + $prepaid);
							}
						?>
					</h1>
				</div>
			</div>

			<div style="background-color: white; padding: 5% 0 5% 0; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px">
				<div class="container-fluid">
					<div class="col-12 row" style="text-align: center">
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="border-right: 1px solid #f4f4f4">
							<h6 style="font-weight: bold">PREPAID</h6>
							<h5><?php 
									if (date("d") <= 10) {
										$sum = $lastmonth_pln['prepaid1'];
										echo number_format($sum);
									}
									elseif (date("d") > 10 && date("d") <= 20) {
										$sum = $lastmonth_pln['prepaid1'] + $lastmonth_pln['prepaid2'];
										echo number_format($sum);
									}
									else{
										$sum = $lastmonth_pln['prepaid1'] + $lastmonth_pln['prepaid2'] + $lastmonth_pln['prepaid3'];
										echo number_format($sum);
									}
									
								 ?></h5>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
							<h6 style="font-weight: bold">POSTPAID</h6>
							<h6><?php 
									if (date("d") <= 10) {
										$sum = $lastmonth_pln['postpaid1'];
										echo number_format($sum);
									}
									elseif (date("d") > 10 && date("d") <= 20) {
										$sum = $lastmonth_pln['postpaid1'] + $lastmonth_pln['postpaid2'];
										echo number_format($sum);
									}
									else{
										$sum = $lastmonth_pln['postpaid1'] + $lastmonth_pln['postpaid2'] + $lastmonth_pln['postpaid3'];
										echo number_format($sum);
									}
							 ?></h6>
						</div>
					</div>
				</div>
			</div>
		</div>












		<!-- This MOnth -->
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="">	
			<div style="background-color: #ff8700; padding: 2% 0 2% 0; border-top-left-radius: 10px; border-top-right-radius: 10px">
				<h5 class="count_top" style="color: white">Total PLN<br>MTD</h5>
				<div class="count">
					<h1 style="font-weight: bold; color: white">
						<?php 
							$prepaid = $thismonth_pln['prepaid1'] + $thismonth_pln['prepaid2'] + $thismonth_pln['prepaid3'];
							$postpaid = $thismonth_pln['postpaid1'] + $thismonth_pln['postpaid2'] + $thismonth_pln['postpaid3'];
							echo number_format($postpaid + $prepaid);
						?>
					</h1>
				</div>
			</div>

			<div style="background-color: white; padding: 5% 0 5% 0; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px">
				<div class="container-fluid">
					<div class="col-12 row" style="text-align: center">
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="border-right: 1px solid #f4f4f4">
							<h6 style="font-weight: bold">PREPAID</h6>
							<h5><?php 
									$sum = $thismonth_pln['prepaid1'] + $thismonth_pln['prepaid2'] + $thismonth_pln['prepaid3'];

									if ($sum > 0) {
										echo number_format($sum);
									}
									else{
										echo 0;
									}
							 ?></h5>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
							<h6 style="font-weight: bold">POSTPAID</h6>
							<h6><?php 
									$sum = $thismonth_pln['postpaid1'] + $thismonth_pln['postpaid2'] + $thismonth_pln['postpaid3'];

									if ($sum > 0) {
										echo number_format($sum);
									}
									else{
										echo 0;
									}
							?></h6>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!-- Today -->
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="padding-right :1%;">	
			<div style="background-color: #ff8700; padding: 2% 0 2% 0; border-top-left-radius: 10px; border-top-right-radius: 10px">
				<h5 class="count_top" style="color: white">Total PLN<br>TODAY</h5>
				<div class="count">
					<h1 style="font-weight: bold; color: white">
						<?php $sum = 0;
							if($pln[0]['total_prepaid']>=0){
								if($pln[0]['total_postpaid']>=0){
									$sum = $pln[0]['total_prepaid'] + $pln[0]['total_postpaid'];
								}else{
									$sum = $pln[0]['total_prepaid'];
								}
							}else if($pln[0]['total_postpaid']>=0){
								$sum = $pln[0]['total_postpaid'];
							}
							echo number_format($sum);
						?>
					</h1>
				</div>
			</div>

			<div style="background-color: white; padding: 5% 0 5% 0; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px">
				<div class="container-fluid">
					<div class="col-12 row" style="text-align: center">
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="border-right: 1px solid #f4f4f4">
							<h6 style="font-weight: bold">PREPAID</h6>
							<h6><?php 
									if($pln[0]['total_prepaid']>=0){
										echo number_format($pln[0]["total_prepaid"]);			
									}else{
										echo 0;
									} 
								?></h5>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
							<h6 style="font-weight: bold">POSTPAID</h6>
							<h6><?php 
									if($pln[0]['total_postpaid']>=0){
										echo number_format($pln[0]["total_postpaid"]);			
									}else{
										echo 0;
									}
								?></h6>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>		

	<!-- PDAM -->
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 3%; padding-left: 0%;padding-right: 0%;">
		<!-- last month -->
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="padding-left: 1%">
			<div style="background-color: #ff8700; padding: 2% 0 2% 0; border-top-left-radius: 10px; border-top-right-radius: 10px">
				<h5 class="count_top" style="color: white">Total PDAM</h5>
				<div class="count">
					<h1 style="font-weight: bold; color: white">
						<?php
							if($lastmonth_pdam[0]["pdamtotal"] >= 0)
							{
								
								echo number_format($lastmonth_pdam[0]["pdamtotal"]);
							}
							else{
								echo 0;
							}
						?>
					</h1>
				</div>
			</div>

			<div style="background-color: white; padding: 5% 0 5% 0; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px">
				<div class="container-fluid">
					<div class="col-12 row" style="text-align: center">
						<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid #f4f4f4">
							<h6 style="font-weight: bold">SUCCESS</h6>
							<h6>
							<?php
								if($lastmonth_pdam[0]["pdamsukses"]>=0){
									echo number_format($lastmonth_pdam[0]["pdamsukses"]);		
								}else{
									echo 0;
								}					
							?>
							</h5>
						</div>
						<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid #f4f4f4">
							<h6 style="font-weight: bold">FAILED</h6>
							<h6>
							<?php
								if($lastmonth_pdam[0]["pdamgagal"]>=0){
									echo number_format($lastmonth_pdam[0]["pdamgagal"]);		
								}else{
									echo 0;
								}
							?>
							</h6>
						</div>
						<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
							<h6 style="font-weight: bold">%</h6>
							<h6>
								<?php $sukses=0; $gagal=0;
									if($lastmonth_pdam[0]['pdamsukses']>=0){
										$sukses = $lastmonth_pdam[0]['pdamsukses'];
									}
									if($lastmonth_pdam[0]['pdamgagal']>=0){
										$gagal = $lastmonth_pdam[0]['pdamgagal'];
									}
									if(($sukses+$gagal)==0){
										echo 0;
									}else{
										echo number_format($sukses/($sukses+$gagal)*100);
									}
								?>
							</h6>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- this month -->
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="">
			<div style="background-color: #ff8700; padding: 2% 0 2% 0; border-top-left-radius: 10px; border-top-right-radius: 10px">
				<h5 class="count_top" style="color: white">Total PDAM</h5>
				<div class="count">
					<h1 style="font-weight: bold; color: white">
						<?php
							if($thismonth_pdam[0]["pdamtotal"] >= 0)
							{
								
								echo number_format($thismonth_pdam[0]["pdamtotal"]);
							}
							else{
								echo 0;
							}
						?>
					</h1>
				</div>
			</div>

			<div style="background-color: white; padding: 5% 0 5% 0; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px">
				<div class="container-fluid">
					<div class="col-12 row" style="text-align: center">
						<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid #f4f4f4">
							<h6 style="font-weight: bold">SUCCESS</h6>
							<h6>
							<?php
								if($thismonth_pdam[0]["pdamsukses"]>=0){
									echo number_format($thismonth_pdam[0]["pdamsukses"]);		
								}else{
									echo 0;
								}					
							?>
							</h5>
						</div>
						<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid #f4f4f4">
							<h6 style="font-weight: bold">FAILED</h6>
							<h6>
							<?php
								if($thismonth_pdam[0]["pdamgagal"]>=0){
									echo number_format($thismonth_pdam[0]["pdamgagal"]);		
								}else{
									echo 0;
								}
							?>
							</h6>
						</div>
						<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
							<h6 style="font-weight: bold">%</h6>
							<h6>
								<?php $sukses=0; $gagal=0;
									if($thismonth_pdam[0]['pdamsukses']>=0){
										$sukses = $thismonth_pdam[0]['pdamsukses'];
									}
									if($thismonth_pdam[0]['pdamgagal']>=0){
										$gagal = $thismonth_pdam[0]['pdamgagal'];
									}
									if(($sukses+$gagal)==0){
										echo 0;
									}else{
										echo number_format($sukses/($sukses+$gagal)*100);
									}
								?>
							</h6>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- today -->
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="padding-right: 1%">
			<div style="background-color: #ff8700; padding: 2% 0 2% 0; border-top-left-radius: 10px; border-top-right-radius: 10px">
				<h5 class="count_top" style="color: white">Total PDAM</h5>
				<div class="count">
					<h1 style="font-weight: bold; color: white">
						<?php
							if($pdam[0]["pdamtotal"] >= 0)
							{
								
								echo number_format($pdam[0]["pdamtotal"]);
							}
							else{
								echo 0;
							}
						?>
					</h1>
				</div>
			</div>

			<div style="background-color: white; padding: 5% 0 5% 0; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px">
				<div class="container-fluid">
					<div class="col-12 row" style="text-align: center">
						<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid #f4f4f4">
							<h6 style="font-weight: bold">SUCCESS</h6>
							<h6>
							<?php
								if($pdam[0]["pdamsukses"]>=0){
									echo number_format($pdam[0]["pdamsukses"]);		
								}else{
									echo 0;
								}					
							?>
							</h5>
						</div>
						<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid #f4f4f4">
							<h6 style="font-weight: bold">FAILED</h6>
							<h6>
							<?php
								if($pdam[0]["pdamgagal"]>=0){
									echo number_format($pdam[0]["pdamgagal"]);		
								}else{
									echo 0;
								}
							?>
							</h6>
						</div>
						<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
							<h6 style="font-weight: bold">%</h6>
							<h6>
								<?php $sukses=0; $gagal=0;
									if($pdam[0]['pdamsukses']>=0){
										$sukses = $pdam[0]['pdamsukses'];
									}
									if($pdam[0]['pdamgagal']>=0){
										$gagal = $pdam[0]['pdamgagal'];
									}
									if(($sukses+$gagal)==0){
										echo 0;
									}else{
										echo number_format($sukses/($sukses+$gagal)*100);
									}
								?>
							</h6>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>