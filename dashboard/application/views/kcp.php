<!-- KCP -->
<div id="kcp" class="col-12 row" style="text-align: center; margin: 0; padding-right: 1%; padding-left: 1%;">	<!-- class=".tile_waris1" -->
	
	<!-- Last Month -->
	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding: 3% 1% 3% 0%">	<!-- class=".tile_waris1_count" -->
		<div style=" background-color: #ff8700; padding: 2% 0 2% 0; border-top-left-radius: 10px; border-top-right-radius: 10px">
			<h5 class="count_top" style="color: white">Total KCP<br>Last Month</h5>
			<div class="count">
				<h1 style="font-weight: bold; color: white">
					<?php 

						$sum = 0;

						for ($i=0; $i < count($last_month) ; $i++) { 
							if (isset($last_month[$i]['CLASS'])) {
								$sum += $last_month[$i]['TOTAL'];
							}		
						}

						echo number_format($sum);
					?>
				</h1>
			</div>
		</div>
				
		<div style="background-color: white; padding: 5% 0 5% 0">
			<div class="container-fluid">
				<div class="col-12 row" style="text-align: center">
					<?php 
						$last_month_valid = array();

						for ($i=0; $i < count($last_month) ; $i++) { 
							if (isset($last_month[$i]['CLASS'])) {
								$last_month_valid[] = $last_month[$i];
							}		
						}

						$a=0; $b=0; $c=0; $d=0;
						foreach ($last_month_valid as $key => $value) {
							if($value['CLASS']=="CLASS A"){
								$a = $value['TOTAL'];
							}else if($value['CLASS']=="CLASS B"){
								$b = $value['TOTAL'];
							}else if($value['CLASS']=="CLASS C"){
								$c = $value['TOTAL'];
							}else if($value['CLASS']=="CLASS D"){
								$d = $value['TOTAL'];
							}

						}

					 ?>
					<div class='col-xs-3 col-sm-3 col-md-3 col-lg-3' style='border-right: 1px solid #f4f4f4'>
										<h6 style='font-weight: bold'>CLASS A<br><span style="opacity: 0.5">(1-10)</span></h6>
										<h6><?php echo number_format($a) ?></h6> </div>
					<div class='col-xs-3 col-sm-3 col-md-3 col-lg-3' style='border-right: 1px solid #f4f4f4'>
										<h6 style='font-weight: bold'>CLASS B<br><span style="opacity: 0.5">(11-50)</span></h6>
										<h6><?php echo number_format($b) ?></h6> </div>
					<div class='col-xs-3 col-sm-3 col-md-3 col-lg-3' style='border-right: 1px solid #f4f4f4'>
										<h6 style='font-weight: bold'>CLASS C<br><span style="opacity: 0.5">(51-100)</span></h6>
										<h6><?php echo number_format($c) ?></h6> </div>
					<div class='col-xs-3 col-sm-3 col-md-3 col-lg-3' style='border-right: 1px solid #f4f4f4'>
										<h6 style='font-weight: bold'>CLASS D<br><span style="opacity: 0.5">(>101)</span></h6>
										<h6><?php echo number_format($d) ?></h6> </div>
				</div>
			</div>
		</div>
	</div>

	<!-- This Month -->
	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding: 3% 0.5% 3% 0.5%">	<!-- class=".tile_waris1_count" -->
		<div style=" background-color: #ff8700; padding: 2% 0 2% 0; border-top-left-radius: 10px; border-top-right-radius: 10px">
			<h5 class="count_top" style="color: white">Total KCP<br>This Month</h5>
			<div class="count">
				<h1 style="font-weight: bold; color: white">
					<?php 

						$sum = 0;

						for ($i=0; $i < count($this_month) ; $i++) { 
							if (isset($this_month[$i]['CLASS'])) {
								$sum += $this_month[$i]['TOTAL'];
							}		
						}

						echo number_format($sum);
					?>
				</h1>
			</div>
		</div>
				
		<div style="background-color: white; padding: 5% 0 5% 0">
			<div class="container-fluid">
				<div class="col-12 row" style="text-align: center">
					<?php 
						$this_month_valid = array();

						for ($i=0; $i < count($this_month) ; $i++) { 
							if (isset($this_month[$i]['CLASS'])) {
								$this_month_valid[] = $this_month[$i];
							}		
						}

						$a=0; $b=0; $c=0; $d=0;
						foreach ($this_month_valid as $key => $value) {
							if($value['CLASS']=="CLASS A"){
								$a = $value['TOTAL'];
							}else if($value['CLASS']=="CLASS B"){
								$b = $value['TOTAL'];
							}else if($value['CLASS']=="CLASS C"){
								$c = $value['TOTAL'];
							}else if($value['CLASS']=="CLASS D"){
								$d = $value['TOTAL'];
							}

						}

					 ?>
					<div class='col-xs-3 col-sm-3 col-md-3 col-lg-3' style='border-right: 1px solid #f4f4f4'>
										<h6 style='font-weight: bold'>CLASS A<br><span style="opacity: 0.5">(1-10)</span></h6>
										<h6><?php echo number_format($a) ?></h6> </div>
					<div class='col-xs-3 col-sm-3 col-md-3 col-lg-3' style='border-right: 1px solid #f4f4f4'>
										<h6 style='font-weight: bold'>CLASS B<br><span style="opacity: 0.5">(11-50)</span></h6>
										<h6><?php echo number_format($b) ?></h6> </div>
					<div class='col-xs-3 col-sm-3 col-md-3 col-lg-3' style='border-right: 1px solid #f4f4f4'>
										<h6 style='font-weight: bold'>CLASS C<br><span style="opacity: 0.5">(51-100)</span></h6>
										<h6><?php echo number_format($c) ?></h6> </div>
					<div class='col-xs-3 col-sm-3 col-md-3 col-lg-3' style='border-right: 1px solid #f4f4f4'>
										<h6 style='font-weight: bold'>CLASS D<br><span style="opacity: 0.5">(>101)</span></h6>
										<h6><?php echo number_format($d) ?></h6> </div>
				</div>
			</div>
		</div>
	</div>	

	<!-- Today -->
	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding: 3% 0% 3% 1%">	<!-- class=".tile_waris1_count" -->
		<div style=" background-color: #ff8700; padding: 2% 0 2% 0; border-top-left-radius: 10px; border-top-right-radius: 10px">
			<h5 class="count_top" style="color: white">Total KCP<br>Today</h5>
			<div class="count">
				<h1 style="font-weight: bold; color: white">
					<?php 

						$sum = 0;

						for ($i=0; $i < count($this_day) ; $i++) { 
							if (isset($this_day[$i]['CLASS'])) {
								$sum += $this_day[$i]['TOTAL'];
							}		
						}

						echo number_format($sum);
					?>
				</h1>
			</div>
		</div>
				
		<div style="background-color: white; padding: 5% 0 5% 0">
			<div class="container-fluid">
				<div class="col-12 row" style="text-align: center">
					<?php 
						$this_day_valid = array();

						for ($i=0; $i < count($this_day) ; $i++) { 
							if (isset($this_day[$i]['CLASS'])) {
								$this_day_valid[] = $this_day[$i];
							}		
						}

						$a=0; $b=0; $c=0; $d=0;
						foreach ($this_day_valid as $key => $value) {
							if($value['CLASS']=="CLASS A"){
								$a = $value['TOTAL'];
							}else if($value['CLASS']=="CLASS B"){
								$b = $value['TOTAL'];
							}else if($value['CLASS']=="CLASS C"){
								$c = $value['TOTAL'];
							}else if($value['CLASS']=="CLASS D"){
								$d = $value['TOTAL'];
							}

						}

					 ?>
					<div class='col-xs-3 col-sm-3 col-md-3 col-lg-3' style='border-right: 1px solid #f4f4f4'>
										<h6 style='font-weight: bold'>CLASS A<br><span style="opacity: 0.5">(1-10)</span></h6>
										<h6><?php echo number_format($a) ?></h6> </div>
					<div class='col-xs-3 col-sm-3 col-md-3 col-lg-3' style='border-right: 1px solid #f4f4f4'>
										<h6 style='font-weight: bold'>CLASS B<br><span style="opacity: 0.5">(11-50)</span></h6>
										<h6><?php echo number_format($b) ?></h6> </div>
					<div class='col-xs-3 col-sm-3 col-md-3 col-lg-3' style='border-right: 1px solid #f4f4f4'>
										<h6 style='font-weight: bold'>CLASS C<br><span style="opacity: 0.5">(51-100)</span></h6>
										<h6><?php echo number_format($c) ?></h6> </div>
					<div class='col-xs-3 col-sm-3 col-md-3 col-lg-3' style='border-right: 1px solid #f4f4f4'>
										<h6 style='font-weight: bold'>CLASS D<br><span style="opacity: 0.5">(>101)</span></h6>
										<h6><?php echo number_format($d) ?></h6> </div>
				</div>
			</div>
		</div>
	</div>
</div>