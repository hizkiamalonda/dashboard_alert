

			<!-- Bank Payment -->
			<div id="top" class="col-12 row" style="text-align: center; margin: 0">	<!-- class=".tile_count" -->
				<!-- 1 -->
            	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="padding: 3% 1% 0 1%">	
					<div style="background-color: #ff8700; padding: 2% 0 2% 0; border-top-left-radius: 10px; border-top-right-radius: 10px; height: 100px">
						<h5 class="count_top" style="color: white; margin-top: 4%">Total Top Up</h5>
						<div class="count">
							<h2 style="font-weight: bold; color: white">
								<?php
									if( $data["total_manual"] || $data["totalticket"] || $data["totalvirtual"] > 0 )
									{
										$jumlah = $data["total_manual"] + $data["totalticket"] + $data["totalvirtual"];
										echo number_format ($jumlah);
									}
									else{
										echo 0;
									}
									
								?>
							</h2>
						</div>
					</div>

					<div style="background-color: white; padding: 4% 0 5% 0; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px; height: 114px">
						<div class="container-fluid">
							<div class="col-12 row" style="text-align: center">
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid #f4f4f4">
									<h6 style="font-weight: bold">Manual</h6>
									<h6>
										<?php
											if( $data["total_manual"] > 0 )
											{
											echo number_format ($data["total_manual"]);
											}
											else{
											echo 0;
											}
										?>
									</h6>
								</div>
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="border-right: 1px solid #f4f4f4">
									<h6 style="font-weight: bold">Ticket</h6>
									<h6>
										<?php
											if( $data["totalticket"] > 0 )
											{
											echo number_format ($data["totalticket"]);
											}
											else{
											echo 0;
											}
										?>
									</h6>
								</div>
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
									<h6 style="font-weight: bold">VA</h6>
									<h6>
										<?php 
											if( $data["totalvirtual"] > 0 )
											{
											  echo number_format ($data["totalvirtual"]);
											}
											else{
											  echo 0;
											}
										?>
									</h6>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<!-- 2 -->
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="padding: 3% 1% 0 1%">	
					<div style="background-color: #ff8700; padding: 2% 0 2% 0; border-top-left-radius: 10px; border-top-right-radius: 10px; height: 100px">
						<h5 class="count_top" style="color: white; margin-top: 3%">Manual Top Up</h5>
						<div class="count">
							<h2 style="font-weight: bold; color: white">
								<?php
									if($data["total_manual"]> 0)
									{
										echo number_format ($data["total_manual"]);
									}
									else{
										echo 0;
									};
								?>
							</h2>
						</div>
					</div>
					
					<div style="background-color: white; padding: 3% 0 5% 0; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px; height: 114px">
						<div class="container-fluid">
							<div class="col-12 row" style="text-align: center">
								<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="border-right: 1px solid #f4f4f4">
									<h6 style="font-weight: bold">BCA</h6>
									<h6>
										<?php
											if( $data["manualbca"] > 0)
											{
											  echo number_format ($data["manualbca"]);
											}
											else{
											  echo 0;
											}	
										?>
									</h6>
								</div>
								<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="border-right: 1px solid #f4f4f4">
									<h6 style="font-weight: bold">Mandiri</h6>
									<h6>
										<?php
											if( $data["manualmandiri"] > 0 )
											{
											echo number_format ($data["manualmandiri"]);
											}
											else{
											echo 0;
											}	
										?>
									</h6>
								</div>
								<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="border-right: 1px solid #f4f4f4">
									<h6 style="font-weight: bold">Permata</h6>
									<h6>
										<?php
											if( $data["manualpermata"] > 0 )
											{
											  echo number_format ($data["manualpermata"]);
											}
											else{
											  echo 0;
											}
										?>
									</h6>
								</div>
								<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="border-right: 1px solid #f4f4f4">
									<h6 style="font-weight: bold">BRI</h6>
									<h6>
										<?php
											if($data["manualbri"] > 0 )
											{
											  echo number_format ($data["manualbri"]);
											}
											else{
											  echo 0;
											}	
										?>
									</h6>
								</div>
								<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
									<h6 style="font-weight: bold">Sinarmas</h6>
									<h6>
										<?php
											if( $data["manualsinarmas"] > 0 )
											{
											  echo number_format ($data["manualsinarmas"]);
											}
											else{
											  echo 0;
											}		
										?>
									</h6>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- 3 -->
            	<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12" style="padding: 3% 1% 0 1%"> 
					<div style="background-color: #ff8700; padding: 2% 0 2% 0; border-top-left-radius: 10px; border-top-right-radius: 10px; height: 100px">
						<h5 class="count_top" style="color: white; margin-top: 4%">Top Up Ticket</h5>
						<div class="count">
							<h2 style="font-weight: bold; color: white">
								<?php
									if( $data["totalticket"] > 0 )
									{
									  echo number_format ($data["totalticket"]);
									}
									else{
									  echo 0;
									}
								?>
							</h2>
						</div>
					</div>
					
					<div style="background-color: white; padding: 6% 0 5% 0; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px; height: 114px">
						<div class="container-fluid">
							<div class="col-12 row" style="text-align: center">
								<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="border-right: 1px solid #f4f4f4">
									<h6 style="font-weight: bold">BCA</h6>
									<h6>
										<?php
											if( $data["tiketbca"] > 0 )
											{ 
												echo number_format ($data["tiketbca"]);
											}
											
											else{
												echo 0;
											}
										?>
									</h6>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
									<h6 style="font-weight: bold">Mandiri</h6>
									<h6>
										<?php
											if( $data["tiketmandiri"] > 0 )
											{ 
												echo number_format ($data["tiketmandiri"]);
											}
											
											else{
												echo 0;
											}
										?>
									</h6>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- 4 -->
            	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="padding: 3% 1% 0 1%">	
					<div style="background-color: #ff8700; padding: 2% 0 2% 0; border-top-left-radius: 10px; border-top-right-radius: 10px; height: 100px">
						<h5 class="count_top" style="color: white; margin-top: 4%">Virtual Account</h5>
						<div class="count">
							<h2 style="font-weight: bold; color: white">
								<?php
									if( $data["totalvirtual"] > 0 )
									{ 
										echo number_format ($data["totalvirtual"]);
									}
									
									else{
										echo 0;
									}
								?>
							</h2>
						</div>
					</div>
					
					<div style="background-color: white; padding: 4% 0 5% 0; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px">
						<div class="container-fluid">
							<div class="col-12 row" style="text-align: center">
								<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="border-right: 1px solid #f4f4f4">
									<h6 style="font-weight: bold">BRI</h6>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
									<h6 style="font-weight: bold">Sinarmas</h6>
								</div>
							</div>	
						</div>

						<div class="container-fluid">
							<div class="col-12 row" style="text-align: center">
								<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="border-right: 1px solid #f4f4f4">
									<h6 style="font-weight: bold">Jml Trx</h6>
									<h6>
										<?php 
											if( $virtual["virtualbri"] > 0 )
											{ 
												echo number_format ($virtual["virtualbri"]);
											}
											
											else{
												echo 0;
											} 
										?>
									</h6>
								</div>
								<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="border-right: 1px solid #f4f4f4">
									<h6 style="font-weight: bold">Value</h6>
									<h6>
										<?php 
											if( $virtual["virtualamountbri"] > 0 )
											{ 
												echo number_format ($virtual["virtualamountbri"]);
											}
											
											else{
												echo 0;
											} 
										?>
									</h6>
								</div>
								<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="border-right: 1px solid #f4f4f4">
									<h6 style="font-weight: bold">Jml Trx</h6>
									<h6>
										<?php 
											if( $virtual["virtualsinarmas"] > 0 )
											{ 
												echo number_format ($virtual["virtualsinarmas"]);
											}
											
											else{
												echo 0;
											}
										?>
									</h6>
								</div>
								<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
									<h6 style="font-weight: bold">Value</h6>
									<h6>
										<?php 
											if( $virtual["virtualamountsinarmas"] > 0 )
											{ 
												echo number_format ($virtual["virtualamountsinarmas"]);
											}
											
											else{
												echo 0;
											}
										?>
									</h6>
								</div>
							</div>
						</div>	
					</div>
				</div>           	
          	</div>
