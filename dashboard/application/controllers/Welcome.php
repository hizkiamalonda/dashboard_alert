<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	// public function index()
	// {
	// 	$this->load->view('welcome_message');
	// }
		
	public function transaksi()
	{
		$this->load->model('Transaksi');// Model Transaksi
		$trans['pulsa_today'] = $this->Transaksi->get_total_today('MR');
		$trans['data_today'] = $this->Transaksi->get_total_today('DC');
		
		$trans['this_month'] = $this->Transaksi->get_total_this_month();
		// $trans['this_month'] = $this->Transaksi->get_total_this_month('DC');
		
		$trans['last_month'] = $this->Transaksi->get_total_last_month();
		// $trans['data_last_month'] = $this->Transaksi->get_total_last_month('DC');
		$this->load->view('transaksi', $trans);
	}


	public function operator()
	{
		// Model Transaksi
		$this->load->model('Operator');

		// $provider['indosat_status'] = $this->Operator->get_operator_status('indosat');
		$provider['indosat_code'] = $this->Operator->get_product_code('indosat');

		// $provider['smartfren_status'] = $this->Operator->get_operator_status('smartfren');
		$provider['smartfren_code'] = $this->Operator->get_product_code('smartfren');

		// $provider['three_status'] = $this->Operator->get_operator_status('three');
		$provider['three_code'] = $this->Operator->get_product_code('three');

		// $provider['xl_status'] = $this->Operator->get_operator_status('xl');
		$provider['xl_code'] = $this->Operator->get_product_code('xl');		
		
		// $provider['bolt_status'] = $this->Operator->get_operator_status('bolt');
		$provider['bolt_code'] = $this->Operator->get_product_code('bolt');

		$provider['telkomsel_code'] = $this->Operator->get_product_code('telkomsel');

		$provider['operator_status'] = $this->Operator->get_operator_status();
		$provider['operator_status_lastmtd'] = $this->Operator->get_operator_status_lastmonth();
		$provider['operator_status_curmtd'] = $this->Operator->get_operator_status_curmonth();
		$this->load->view('operator' , $provider);
	}




	//Get all data for topup manual and topup virtual
	public function topup(){
		$this->load->model('Topup');

		$obj = $this->Topup->get_all();
		$obj_virtual = $this->Topup->get_virtual();

		if (!$obj || !$obj_virtual) {
			echo "<marquee><h2>DATABASE TOP UP HAS NO DATA</h2></marquee>";
		}
		else{
			$data['data'] = $obj[0];
			$data['virtual'] = $obj_virtual[0];

			$this->load->view('topup' , $data);
		}
		

		

	}


	public function plnpdam(){
		$this->load->model('Transaksi');
		// $pln['pln'] = $this->Transaksi->get_PLN();
		// $pln['thismonth'] = $this->Transaksi->PLN_thismonth();
		// $pln['postthismonth'] = $this->Transaksi->PLN_thismonth('thispostpaid','PLN Postpaid');
		// $pln['lastmonth'] = $this->Transaksi->PLN_lastmonth();
		// $pln['postlastmonth'] = $this->Transaksi->PLN_lastmonth('lastpostpaid','PLN Postpaid');
		// $pln['pdam'] = $this->Transaksi->get_PDAM();


		$pln['pdam'] = $this->Transaksi->get_PDAM();
		$pln['thismonth_pdam'] = $this->Transaksi->PDAM_thismonth();
		$pln['lastmonth_pdam'] = $this->Transaksi->PDAM_lastmonth();

		$pln['pln'] = $this->Transaksi->get_PLN();

		$pln_this_month = $this->Transaksi->PLN_thismonth();
		$pln['thismonth_pln'] = $pln_this_month[0];

		if (date("d") <=10) {
			$pln_last_month = $this->Transaksi->PLN_lastmonth_10();
			$pln['lastmonth_pln'] = $pln_last_month[0];
		}
		elseif ( date("d") <= 20 && date("d") > 10) {
			$pln_last_month = $this->Transaksi->PLN_lastmonth_20();
			$pln['lastmonth_pln'] = $pln_last_month[0];
		}
		else{
			$pln_last_month = $this->Transaksi->PLN_lastmonth();
			$pln['lastmonth_pln'] = $pln_last_month[0];
		}

		$this->load->view('plnpdam' , $pln);
	}



	// Get Data Kcp From Local Database
	public function kcp()
	{
		$this->load->model('Totalkcp');
		$class['this_day']=$this->Totalkcp->get_class_this_day();
		$class['this_month']=$this->Totalkcp->get_class_this_month();
		$class['last_month']=$this->Totalkcp->get_class_last_month();
		$this->load->view('kcp', $class);

	}


	public function index()
	{
		$this->transaksi();	
		$this->operator();
		$this->topup();
		$this->plnpdam();
		$this->kcp();

		$this->load->view('dashboard');
	}

}
